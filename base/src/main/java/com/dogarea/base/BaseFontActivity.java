package com.dogarea.base;

import android.content.Context;
import android.support.annotation.CallSuper;

import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * BaseActivity with support for vector drawables, butterknife and custom fonts through Calligraphy library
 */
public class BaseFontActivity extends BaseActivity {
    @Override
    @CallSuper
    public void onContentChanged() {
        super.onContentChanged();
        ButterKnife.bind(this);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
