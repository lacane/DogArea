package com.dogarea.base;

import android.content.Intent;
import android.support.annotation.CallSuper;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;

import butterknife.ButterKnife;

/**
 * BaseActivity with support for vector drawables and butterknife
 */
public class BaseActivity extends AppCompatActivity {
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @Override
    @CallSuper
    public void onContentChanged() {
        super.onContentChanged();
        ButterKnife.bind(this);
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
//        overridePendingTransition(R.anim.bottom_in, R.anim.bottom_out);
    }
    @Override
    public void finish() {
        super.finish();
//        overridePendingTransition(R.anim.top_in, R.anim.top_out);
    }
}
