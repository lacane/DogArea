package com.dogarea.base.mvp.view;

import com.alapshin.arctor.view.MvpView;

public interface ErrorMvpView extends MvpView {
    void onError(Throwable error);
}
