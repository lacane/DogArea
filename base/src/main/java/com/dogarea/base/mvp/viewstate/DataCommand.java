package com.dogarea.base.mvp.viewstate;

import com.alapshin.arctor.view.MvpView;
import com.alapshin.arctor.viewstate.ViewStateCommand;

public abstract class DataCommand<T, V extends MvpView> implements ViewStateCommand<V> {
    public static final int COMMAND_TYPE = 0;

    @Override
    public int type() {
        return COMMAND_TYPE;
    }

    public abstract T data();
}
