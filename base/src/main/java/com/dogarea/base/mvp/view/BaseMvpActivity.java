package com.dogarea.base.mvp.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;

import com.alapshin.arctor.presenter.Presenter;
import com.alapshin.arctor.view.MvpActivity;
import com.alapshin.arctor.view.MvpView;
import com.dogarea.base.R;
import com.dogarea.base.di.ComponentCache;
import com.dogarea.base.di.ComponentCacheDelegate;
import com.trello.rxlifecycle.ActivityEvent;
import com.trello.rxlifecycle.ActivityLifecycleProvider;
import com.trello.rxlifecycle.LifecycleTransformer;
import com.trello.rxlifecycle.RxLifecycle;

import butterknife.ButterKnife;
import rx.Observable;
import rx.subjects.BehaviorSubject;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public abstract class BaseMvpActivity<V extends MvpView, P extends Presenter<V>>
        extends MvpActivity<V, P> implements ActivityLifecycleProvider, ComponentCache {

    private final ComponentCacheDelegate componentCacheDelegate = new ComponentCacheDelegate();
    private final BehaviorSubject<ActivityEvent> lifecycleSubject = BehaviorSubject.create();

    @Override
    public final Observable<ActivityEvent> lifecycle() {
        return lifecycleSubject.asObservable();
    }

    @Override
    public final <T> LifecycleTransformer<T> bindUntilEvent(@NonNull ActivityEvent event) {
        return RxLifecycle.bindUntilEvent(lifecycleSubject, event);
    }

    @Override
    public final <T> LifecycleTransformer<T> bindToLifecycle() {
        return RxLifecycle.bindActivity(lifecycleSubject);
    }

    @Override
    @CallSuper
    protected void onCreate(Bundle savedInstanceState) {
        // {@link ComponentCacheDelegate#onCreate} should be called before {@link Activity#onCreate}
        componentCacheDelegate.onCreate(savedInstanceState, getLastCustomNonConfigurationInstance());

        injectDependencies();
        setContentView(getLayoutRes());
        super.onCreate(savedInstanceState);
        lifecycleSubject.onNext(ActivityEvent.CREATE);
    }

    @Override
    @CallSuper
    protected void onStart() {
        super.onStart();
        lifecycleSubject.onNext(ActivityEvent.START);
    }

    @Override
    @CallSuper
    protected void onResume() {
        super.onResume();
        lifecycleSubject.onNext(ActivityEvent.RESUME);
    }

    @Override
    @CallSuper
    protected void onPause() {
        lifecycleSubject.onNext(ActivityEvent.PAUSE);
        super.onPause();
    }

    @Override
    @CallSuper
    protected void onStop() {
        lifecycleSubject.onNext(ActivityEvent.STOP);
        super.onStop();
    }

    @Override
    @CallSuper
    protected void onDestroy() {
        lifecycleSubject.onNext(ActivityEvent.DESTROY);
        super.onDestroy();
    }

    @Override
    @CallSuper
    public void onContentChanged() {
        super.onContentChanged();
        ButterKnife.bind(this);
    }

    @Override
    @CallSuper
    public Object onRetainCustomNonConfigurationInstance() {
        return componentCacheDelegate.onRetainCustomNonConfigurationInstance();
    }

    @Override
    public long generateComponentId() {
        return componentCacheDelegate.generateId();
    }

    @Override
    public <C> C getComponent(long index) {
        return componentCacheDelegate.getComponent(index);
    }

    @Override
    public <C> void setComponent(long index, C component) {
        componentCacheDelegate.setComponent(0, component);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(R.anim.bottom_in, R.anim.bottom_out);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.top_in, R.anim.top_out);
    }

    @LayoutRes
    protected abstract int getLayoutRes();
    protected abstract void injectDependencies();
}
