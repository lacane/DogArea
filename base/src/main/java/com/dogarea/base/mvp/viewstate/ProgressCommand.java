package com.dogarea.base.mvp.viewstate;

import android.support.annotation.NonNull;

import com.alapshin.arctor.viewstate.ViewStateCommand;
import com.dogarea.base.mvp.view.ProgressMvpView;
import com.google.auto.value.AutoValue;

/**
 * {@link ViewStateCommand} implementation to show progress indicator
 * @param <V> view type
 */
@AutoValue
public abstract class ProgressCommand<V extends ProgressMvpView> implements ViewStateCommand<V> {
    public static final int COMMAND_TYPE = 3;

    @Override
    public int type() {
        return COMMAND_TYPE;
    }

    @Override
    public void execute(V v) {
        v.onProgress();
    }

    @NonNull
    public static <V extends ProgressMvpView> ProgressCommand<V> create() {
        return new AutoValue_ProgressCommand<V>();
    }
}
