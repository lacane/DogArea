package com.iv.dogarea.common.dogPreview.view;


import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.iv.dogarea.R;
import com.iv.dogarea.common.GenderSwitchView;
import com.iv.dogarea.common.utils.DAUtils;
import com.iv.dogarea.common.utils.DateTimeConvert;
import com.iv.dogarea.user.models.dogs.Dog;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DogItem extends FrameLayout {
    private int index;
    private DogPreview.OnClickDogItemListener onClickDogItemListener;

    public DogItem(Context context) {
        super(context);
        init();
    }
    public DogItem(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    public DogItem(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @BindView(R.id.dog_item_card) CardView cardView;
    @BindView(R.id.dog_item_image) ImageView image;
    @BindView(R.id.dog_item_name_age) TextView nameAge;
    @BindView(R.id.dog_item_race) TextView race;
    @BindView(R.id.dog_item_gender) GenderSwitchView gender;
    @BindView(R.id.dog_item_view_clicked) FrameLayout clicked;

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.view_dog_item, this);
        ButterKnife.bind(this);
    }

    public void setDogModel(Dog dog) {
        if (dog.name() != null) {
            nameAge.setText(DAUtils.getNameAge(dog.name(),
                    DateTimeConvert.getAgeMonths(getContext(), dog.dateOfBirth())));
        }

        if (dog.race() != null)
            race.setText(dog.race());

        if (dog.photo() != null) {
            Picasso.with(getContext())
                    .load(Uri.parse(dog.photo()))
                    .placeholder(R.drawable.dog_placeholder_2)
                    .centerCrop()
                    .fit()
                    .into(image);
        }

        gender.setPreviewMode(dog.gender());

    }

    public void setIndex(int index) {
        this.index = index;
        clicked.setOnClickListener(view -> {
            if (onClickDogItemListener != null)
                onClickDogItemListener.onClick(this.index);
        });
    }

    public void setOnClickDogItemListener(DogPreview.OnClickDogItemListener onClickDogItemListener) {
        this.onClickDogItemListener = onClickDogItemListener;
    }
}
