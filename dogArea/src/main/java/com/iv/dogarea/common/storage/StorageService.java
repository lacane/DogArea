package com.iv.dogarea.common.storage;


import android.content.Context;

import com.iv.dogarea.user.models.events.Event;
import com.iv.dogarea.user.models.dogs.Dog;
import com.iv.dogarea.user.models.User;
import com.orhanobut.hawk.Hawk;
import com.orhanobut.hawk.NoEncryption;

import java.util.ArrayList;
import java.util.List;

public class StorageService implements StorageServiceConst {

    public static void instance(Context context) {
        Hawk.init(context)
                .setEncryption(new NoEncryption())
                .build();
    }

    public static void storeUser(User user) {
        Hawk.put(USER, user);
    }

    public static User getUser() {
        return Hawk.get(USER, null);
    }

    public static void storeEvents(List<Event> events) {
        Hawk.put(EVENTS, events);
    }

    public static void removeEvents() {
        Hawk.delete(EVENTS);
    }

    public static List<Event> getEvents() {
        return Hawk.get(EVENTS, null);
    }

    public static List<Dog> getDogs() {
        return Hawk.get(DOGS, new ArrayList<>());
    }

    public static void storeDogs(List<Dog> dogs) {
        Hawk.put(DOGS, dogs);
    }

}
