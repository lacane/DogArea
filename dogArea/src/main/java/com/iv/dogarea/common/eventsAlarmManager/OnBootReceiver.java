package com.iv.dogarea.common.eventsAlarmManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.iv.dogarea.common.alarmManager.DAAlarmManager;
import com.iv.dogarea.common.storage.StorageServiceConst;
import com.iv.dogarea.user.models.events.Event;

public class OnBootReceiver extends BroadcastReceiver implements StorageServiceConst {

    @Override
    public void onReceive(Context context, Intent intent) {
        DAAlarmManager manager = new DAAlarmManager(context);

        String type = intent.getStringExtra(NOTIFICATION_TYPE);
        if (type == null)
            return;

        switch (type) {
            case EVENT:
                setEventAlarm(intent, manager);
                break;
        }
    }

    private void setEventAlarm(Intent intent, DAAlarmManager manager) {
        Event event = (Event) intent.getSerializableExtra(EVENT);
        if (event != null)
            manager.setEventAlarm(event, intent);
    }
}
