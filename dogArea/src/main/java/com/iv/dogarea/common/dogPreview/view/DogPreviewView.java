package com.iv.dogarea.common.dogPreview.view;


import com.alapshin.arctor.view.MvpView;
import com.iv.dogarea.user.models.dogs.Dog;

import java.util.List;

public interface DogPreviewView extends MvpView {
    void setupDogsData(List<Dog> dogs);
}
