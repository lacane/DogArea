package com.iv.dogarea.common.eventsAlarmManager;


import android.app.Activity;
import android.content.Intent;

import com.iv.dogarea.common.storage.StorageServiceConst;
import com.iv.dogarea.user.models.events.Event;

public class EventAlarmManager implements StorageServiceConst {

    public static void createNewEvent(Activity activity, Event event) {
        Intent intent = new Intent(activity, OnBootReceiver.class);
        intent.putExtra(NOTIFICATION_TYPE, EVENT);
        intent.putExtra(EVENT, event);
        activity.sendBroadcast(intent);
    }
}
