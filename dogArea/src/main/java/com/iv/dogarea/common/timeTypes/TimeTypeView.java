package com.iv.dogarea.common.timeTypes;


import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iv.dogarea.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TimeTypeView extends FrameLayout {

    @BindView(R.id.view_time_type_background) LinearLayout timeBackground;
    @BindView(R.id.view_time_type_divider) View divider;
    @BindView(R.id.time_type_view_label) TextView label;
    @BindView(R.id.time_type_view_text) TextView text;

    private String textString;
    private String labelString;

    private boolean correctValues;

    public TimeTypeView(Context context) {
        super(context);
        init();
    }

    public TimeTypeView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TimeTypeView);
        labelString = a.getString(R.styleable.TimeTypeView_label_time);
        textString = a.getString(R.styleable.TimeTypeView_text_time);
        a.recycle();

        init();
    }

    public TimeTypeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.TimeTypeView, defStyleAttr, 0);
        labelString = a.getString(R.styleable.TimeTypeView_label_time);
        textString = a.getString(R.styleable.TimeTypeView_text_time);
        a.recycle();

        init();
    }

    public void setLabel(String labelString) {
        label.setText(labelString);
    }

    public void setText(String textString) {
        text.setText(textString);
        correctValues = true;
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.view_time_type_view, this);
        ButterKnife.bind(this);

        correctValues = false;

        if (labelString != null)
            setLabel(labelString);

        if (textString != null)
            setText(textString);
    }

    public boolean isCorrectValues() {
        return correctValues;
    }
}
