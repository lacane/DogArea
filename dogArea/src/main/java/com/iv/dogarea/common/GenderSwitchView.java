package com.iv.dogarea.common;


import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.iv.dogarea.R;
import com.iv.dogarea.user.models.Gender;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GenderSwitchView extends FrameLayout {

    @BindView(R.id.gender_switch_female) FrameLayout female;
    @BindView(R.id.gender_switch_male) FrameLayout male;
    @BindView(R.id.gender_switch_root) LinearLayout root;

    private Animation increase;
    private Animation reduce;
    private boolean checked;
    private OnChangeStateListener changeStateListener;

    public GenderSwitchView(Context context) {
        super(context);
        init();
    }

    public GenderSwitchView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GenderSwitchView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.view_gender_switch, this);
        ButterKnife.bind(this);

        checked = false;

        increase = AnimationUtils.loadAnimation(getContext(), R.anim.increase);
        increase.setFillEnabled(true);
        increase.setFillAfter(true);
        reduce = AnimationUtils.loadAnimation(getContext(), R.anim.reduce);
        reduce.setFillEnabled(true);
        reduce.setFillAfter(true);

        male.setOnClickListener(view -> {
            if (!male.isActivated()) {
                checked = true;

                male.setActivated(true);
                female.setActivated(false);
                changeHeight();
            }
        });
        female.setOnClickListener(view -> {
            if (!female.isActivated()) {
                checked = true;

                female.setActivated(true);
                male.setActivated(false);
                changeHeight();
            }
        });
    }

    private void changeHeight(View view, boolean isActivated) {
        if (isActivated)
            view.startAnimation(increase);
        else
            view.startAnimation(reduce);

    }

    private void changeState() {
        if (changeStateListener != null)
            changeStateListener.onChange(getState());
    }

    public void setState(Gender gender) {
        if (gender == Gender.M) {
            male.setActivated(true);
            female.setActivated(false);
        } else {
            male.setActivated(false);
            female.setActivated(true);
        }
        checked = true;
        changeHeight();
    }

    private void changeHeight() {
        changeState();
        changeHeight(male, male.isActivated());
        changeHeight(female, female.isActivated());
    }

    public Gender getState() {
        if (male.isActivated())
            return Gender.M;
        else
            return Gender.W;
    }

    public void setPreviewMode(Gender gender) {
        male.setOnClickListener(null);
        female.setOnClickListener(null);

        if (gender == Gender.M)
            female.setVisibility(GONE);
        else
            male.setVisibility(GONE);
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChangeStateListener(OnChangeStateListener changeStateListener) {
        this.changeStateListener = changeStateListener;
    }

    public interface OnChangeStateListener{
        void onChange(Gender gender);
    }
}
