package com.iv.dogarea.common.utils;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;

import com.iv.dogarea.R;
import com.yalantis.ucrop.UCrop;

import java.io.File;

import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import pl.aprilapps.easyphotopicker.EasyImageConfig;

public class ImageUtils {

    public static void openChooser(Fragment fragment) {
        EasyImage.openChooserWithGallery(fragment,
                fragment.getActivity().getString(R.string.choose_gallery), EasyImageConfig.REQ_SOURCE_CHOOSER);
    }
    public static void openChooser(Activity activity) {
        EasyImage.openChooserWithGallery(activity,
                activity.getString(R.string.choose_gallery), EasyImageConfig.REQ_SOURCE_CHOOSER);
    }

    public static void handleActivityResult(int requestCode, int resultCode, Intent data,
                                            Activity activity) {
        handleActivityResult(requestCode, resultCode, data, activity, true);
    }
    public static void handleActivityResult(int requestCode, int resultCode, Intent data,
                                            Activity activity, boolean isUser) {
        EasyImage.handleActivityResult(requestCode, resultCode, data, activity, new DefaultCallback() {
            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                float x = 3;
                float y = 2;
                if (isUser) {
                    x = 1;
                    y = 1;
                }
                UCrop.of(Uri.fromFile(imageFile), Uri.fromFile(imageFile))
                        .withAspectRatio(x, y)
                        .start(activity);
            }
        });

    }
    public static void handleActivityResult(int requestCode, int resultCode, Intent data,
                                            Fragment fragment, boolean isUser) {
        EasyImage.handleActivityResult(requestCode, resultCode, data, fragment.getActivity(), new DefaultCallback() {
            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                float x = 3;
                float y = 2;
                if (isUser) {
                    x = 1;
                    y = 1;
                }
                UCrop.of(Uri.fromFile(imageFile), Uri.fromFile(imageFile))
                        .withAspectRatio(x, y)
                        .start(fragment.getActivity(), fragment);

            }
        });

    }
    public static void handleActivityResult(int requestCode, int resultCode, Intent data,
                                            Fragment fragment) {
        handleActivityResult(requestCode, resultCode, data, fragment, true);
    }
}
