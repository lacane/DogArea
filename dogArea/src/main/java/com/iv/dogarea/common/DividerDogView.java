package com.iv.dogarea.common;


import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.iv.dogarea.R;

public class DividerDogView extends FrameLayout {
    public DividerDogView(Context context) {
        super(context);
        init();
    }

    public DividerDogView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DividerDogView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.view_dog_divider, this);
    }
}
