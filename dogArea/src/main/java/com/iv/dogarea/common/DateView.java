package com.iv.dogarea.common;


import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.iv.dogarea.R;
import com.iv.dogarea.common.utils.DateTimeConvert;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DateView extends FrameLayout {

    @BindView(R.id.date_view_label) TextView label;
    @BindView(R.id.date_view_text) TextView text;

    private String textString;
    private String labelString;

    public DateView(Context context) {
        super(context);
        init();
    }

    public DateView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.DateView);
        labelString = a.getString(R.styleable.DateView_label_date_view);
        textString = a.getString(R.styleable.DateView_text_date_view);
        a.recycle();

        init();
    }

    public DateView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CircleView);
        labelString = a.getString(R.styleable.CircleView_label_circle);
        textString = a.getString(R.styleable.CircleView_text_circle);
        a.recycle();

        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.view_date_view, this);
        ButterKnife.bind(this);

        if (textString != null)
            text.setText(textString);

        if (labelString != null)
            label.setText(labelString);
    }

    public void setDate(Date date) {
        if (date == null)
            setVisibility(GONE);
        else
            text.setText(DateTimeConvert.getStringShortDate(date));
    }
}
