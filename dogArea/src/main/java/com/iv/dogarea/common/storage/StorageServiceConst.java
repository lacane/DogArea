package com.iv.dogarea.common.storage;


public interface StorageServiceConst {
    String USER = "user";
    String EVENTS = "events";
    String EVENT = "event";
    String DOGS = "dogs";
    String NOTIFICATION_TYPE = "notification_type";
}
