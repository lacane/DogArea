package com.iv.dogarea.common.dialog;


import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.iv.dogarea.R;

public class InfoDialog  {

    private static DialogInterface.OnClickListener defaultListener = (dialogInterface, i) -> {};

    public static void show(Context context, String title, String message) {
        show(context, title, message, null, null, null, null);
    }

    public static void show(Context context, String title, String message,
                            String positiveButton, String negativeButton,
                            DialogInterface.OnClickListener positiveListener,
                            DialogInterface.OnClickListener negativeListener) {
        AlertDialog.Builder adb = new AlertDialog.Builder(context);

        adb.setTitle(title)
                .setMessage(message);

        if (positiveButton != null) {
            if (positiveListener != null)
                adb.setPositiveButton(positiveButton, positiveListener);
            else
                adb.setPositiveButton(positiveButton, defaultListener);
        } else
            adb.setPositiveButton(context.getString(R.string.ok), defaultListener);

        if (negativeButton != null) {
            if (negativeListener != null)
                adb.setNegativeButton(negativeButton, negativeListener);
            else
                adb.setNegativeButton(negativeButton, defaultListener);
        }

        adb.show();
    }
}
