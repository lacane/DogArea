package com.iv.dogarea.common.utils;


import android.content.Context;

import com.iv.dogarea.R;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.Period;
import org.joda.time.PeriodType;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateTimeConvert {

    static SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
    static SimpleDateFormat dateShortFormat = new SimpleDateFormat("dd MMM yyyy", Locale.getDefault());
    static SimpleDateFormat dayMonthFormat = new SimpleDateFormat("dd MMM", Locale.getDefault());
    static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
    static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("dd MMMM HH:mm", Locale.getDefault());
    static SimpleDateFormat fullDateTimeFormat = new SimpleDateFormat("dd MMMM yyyy HH:mm", Locale.getDefault());

    public static String getStringDate(long date) {
        return getStringDate(new Date(date));
    }
    public static String getStringDate(Date date) {
        String dateString = "";

        if (date != null)
            dateString = dateFormat.format(date);

        return dateString;
    }

    public static String getStringDateMonth(long date) {
        return getStringDateMonth(new Date(date));
    }
    public static String getStringDateMonth(Date date) {
        String dateString = "";

        if (date != null)
            dateString = dayMonthFormat.format(date);

        return dateString;
    }

    public static String getStringDateTime(Date date) {
        String dateString = "";

        if (date != null)
            dateString = dateTimeFormat.format(date);

        return dateString;
    }

    public static String getStringTime(Date date) {
        String dateString = "";

        if (date != null)
            dateString = timeFormat.format(date);

        return dateString;
    }

    public static String getStringFullDate(Date date) {
        String dateString = "";

        if (date != null)
            dateString = fullDateTimeFormat.format(date);

        return dateString;
    }

    public static String getStringShortDate(Date date) {
        String dateString = "";

        if (date != null)
            dateString = dateShortFormat.format(date);

        return dateString;
    }

    public static Date getDateFromString(String string) {
        try {
            return dateFormat.parse(string);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getAge(Context context, Date dobDate) {
        String ageString = "";

        if (dobDate != null) {
            Period period = new Period(dobDate.getTime(),
                    DateTime.now().getMillis(), PeriodType.years());

            int age = period.getYears();

            if (age > 0) {
                ageString = String.valueOf(age);
                ageString += " " + getAgesString(context, age);
            }
        }

        return ageString;
    }

    private static String getAgesString(Context context, int age) {
        String ageString = "";

        if (age >= 11 && age <= 19)
            ageString += context.getString(R.string.ages);
        else {
            int diff = age % 10;
            if (diff == 1)
                ageString += context.getString(R.string.age);
            else if (diff >= 2 && diff <= 4)
                ageString += context.getString(R.string.age_2);
            else
                ageString += context.getString(R.string.ages);
        }

        return ageString;
    }

    public static String getAgeMonths(Context context, Date dobDate) {
        String ageString = "";

        if (dobDate != null) {
            Period period = new Period(dobDate.getTime(),
                    DateTime.now().getMillis(), PeriodType.yearMonthDay());

            int age = period.getYears();
            int month = period.getMonths();

            if (age > 0) {
                ageString = String.valueOf(age);
                ageString += " " + getAgesString(context, age);
            }

            if (month > 0) {
                ageString += " " + String.valueOf(month);
                ageString += " " + getMonthsString(context, month);
            }

        }

        return ageString;
    }

    private static String getMonthsString(Context context, int months) {
        String monthsString = "";

        if (months >= 11 && months <= 19)
            monthsString += context.getString(R.string.months_2);
        else {
            int diff = months % 10;
            if (diff == 1)
                monthsString += context.getString(R.string.month);
            else if (diff >= 2 && diff <= 4)
                monthsString += context.getString(R.string.months);
            else
                monthsString += context.getString(R.string.months_2);
        }


        return monthsString;
    }

    public static String getTime(int hour, int minute) {
        String time = "";

        if (hour >= 10)
            time += hour;
        else
            time += "0" + hour;

        time += ":";
        if (minute >= 10)
            time += minute;
        else
            time += "0" + minute;

        return time;
    }


}
