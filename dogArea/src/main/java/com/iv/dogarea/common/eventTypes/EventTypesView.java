package com.iv.dogarea.common.eventTypes;


import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.iv.dogarea.R;
import com.iv.dogarea.user.models.events.EventType;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EventTypesView extends FrameLayout {

    @BindView(R.id.event_types_walk) EventTypeView walk;
    @BindView(R.id.event_types_feed) EventTypeView feed;
    @BindView(R.id.event_types_clinic) EventTypeView clinic;
    @BindView(R.id.event_types_deworming) EventTypeView deworming;
    @BindView(R.id.event_types_vaccination) EventTypeView vaccination;
    @BindView(R.id.event_types_shop) EventTypeView shop;
    @BindView(R.id.event_types_hunter) EventTypeView hunter;
    @BindView(R.id.event_types_workout) EventTypeView workout;
    @BindView(R.id.event_types_grooming) EventTypeView grooming;
    @BindView(R.id.event_types_therapy) EventTypeView therapy;
    @BindView(R.id.event_types_documents) EventTypeView documents;
    @BindView(R.id.event_types_other) EventTypeView other;

    private boolean oneChecked;
    private OnClickListener listener = view -> {
        if (oneChecked)
            disableCheckBoxes(view);
    };

    public EventTypesView(Context context) {
        super(context);
        init();
    }

    public EventTypesView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.EventTypesView);
        oneChecked = a.getBoolean(R.styleable.EventTypesView_one_checked, false);
        a.recycle();

        init();
    }

    public EventTypesView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.EventTypesView, defStyleAttr, 0);
        oneChecked = a.getBoolean(R.styleable.EventTypesView_one_checked, false);
        a.recycle();


        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.view_event_types, this);
        ButterKnife.bind(this);

        setupListeners();
        setupTypes();

        if (oneChecked)
            walk.setChecked(true);
    }

    private void setupTypes() {
        walk.setType(EventType.WALK, oneChecked);
        feed.setType(EventType.FEED, oneChecked);
        clinic.setType(EventType.CLINIC, oneChecked);
        deworming.setType(EventType.DEWORMING, oneChecked);
        vaccination.setType(EventType.VACCINATION, oneChecked);
        shop.setType(EventType.SHOP, oneChecked);
        hunter.setType(EventType.HUNTER, oneChecked);
        workout.setType(EventType.WORKOUT, oneChecked);
        grooming.setType(EventType.GROOMING, oneChecked);
        therapy.setType(EventType.THERAPY, oneChecked);
        documents.setType(EventType.DOCUMENTS, oneChecked);
        other.setType(EventType.OTHER, oneChecked);
    }

    private void disableCheckBoxes(View view) {
        for (EventTypeView typeView: getTypeViews()) {
            if (typeView.getId() != view.getId())
                typeView.setChecked(false);
        }
    }

    private ArrayList<EventTypeView> getTypeViews() {
        ArrayList<EventTypeView> checkBoxes = new ArrayList<>();
        checkBoxes.add(clinic);
        checkBoxes.add(deworming);
        checkBoxes.add(documents);
        checkBoxes.add(feed);
        checkBoxes.add(grooming);
        checkBoxes.add(hunter);
        checkBoxes.add(other);
        checkBoxes.add(shop);
        checkBoxes.add(therapy);
        checkBoxes.add(vaccination);
        checkBoxes.add(walk);
        checkBoxes.add(workout);
        return checkBoxes;
    }

    private void setupListeners() {
        walk.setOnClickListener(listener);
        feed.setOnClickListener(listener);
        clinic.setOnClickListener(listener);
        deworming.setOnClickListener(listener);
        vaccination.setOnClickListener(listener);
        shop.setOnClickListener(listener);
        hunter.setOnClickListener(listener);
        workout.setOnClickListener(listener);
        grooming.setOnClickListener(listener);
        therapy.setOnClickListener(listener);
        documents.setOnClickListener(listener);
        other.setOnClickListener(listener);
    }

    public EventType getCheckedType() {
        for (EventTypeView typeView: getTypeViews()) {
            if (typeView.isChecked())
                return typeView.getType();
        }

        return EventType.OTHER;
    }

    public void setCheckedType(EventType name) {
        switch (name) {
            case OTHER:
                other.setChecked(true);
                disableCheckBoxes(other);
                break;

            case WALK:
                walk.setChecked(true);
                disableCheckBoxes(walk);
                break;

            case FEED:
                feed.setChecked(true);
                disableCheckBoxes(feed);
                break;

            case CLINIC:
                clinic.setChecked(true);
                disableCheckBoxes(clinic);
                break;

            case DEWORMING:
                deworming.setChecked(true);
                disableCheckBoxes(deworming);
                break;

            case VACCINATION:
                vaccination.setChecked(true);
                disableCheckBoxes(vaccination);
                break;

            case SHOP:
                shop.setChecked(true);
                disableCheckBoxes(shop);
                break;

            case HUNTER:
                hunter.setChecked(true);
                disableCheckBoxes(hunter);
                break;

            case WORKOUT:
                workout.setChecked(true);
                disableCheckBoxes(workout);
                break;

            case GROOMING:
                grooming.setChecked(true);
                disableCheckBoxes(grooming);
                break;

            case THERAPY:
                therapy.setChecked(true);
                disableCheckBoxes(therapy);
                break;

            case DOCUMENTS:
                documents.setChecked(true);
                disableCheckBoxes(documents);
                break;

            default:
                break;
        }
    }
}
