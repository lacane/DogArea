package com.iv.dogarea.common;


import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.iv.dogarea.R;
import com.iv.dogarea.user.models.events.WeekType;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WeekView extends FrameLayout {

    @BindView(R.id.view_week_mo) FrameLayout monday;
    @BindView(R.id.view_week_tu) FrameLayout tuesday;
    @BindView(R.id.view_week_we) FrameLayout wednesday;
    @BindView(R.id.view_week_th) FrameLayout thursday;
    @BindView(R.id.view_week_fr) FrameLayout friday;
    @BindView(R.id.view_week_sa) FrameLayout saturday;
    @BindView(R.id.view_week_su) FrameLayout sunday;

    private boolean previewMode;

    private OnClickListener listener = view -> {
        view.setActivated(!view.isActivated());
    };

    public WeekView(Context context) {
        super(context);
    }

    public WeekView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.WeekView);
        previewMode = a.getBoolean(R.styleable.WeekView_preview_week, false);
        a.recycle();

        init();
    }

    public WeekView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.WeekView, defStyleAttr, 0);
        previewMode = a.getBoolean(R.styleable.WeekView_preview_week, false);
        a.recycle();

        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.view_week, this);
        ButterKnife.bind(this);

        if (!previewMode) {
            monday.setOnClickListener(listener);
            tuesday.setOnClickListener(listener);
            wednesday.setOnClickListener(listener);
            thursday.setOnClickListener(listener);
            friday.setOnClickListener(listener);
            saturday.setOnClickListener(listener);
            sunday.setOnClickListener(listener);
        }
    }

    public List<WeekType> getWeekTypes() {
        ArrayList<WeekType> weekTypes = new ArrayList<>();

        if (monday.isActivated())
            weekTypes.add(WeekType.MONDAY);

        if (tuesday.isActivated())
            weekTypes.add(WeekType.TUESDAY);

        if (wednesday.isActivated())
            weekTypes.add(WeekType.WEDNESDAY);

        if (thursday.isActivated())
            weekTypes.add(WeekType.THURSDAY);

        if (friday.isActivated())
            weekTypes.add(WeekType.FRIDAY);

        if (saturday.isActivated())
            weekTypes.add(WeekType.SATURDAY);

        if (sunday.isActivated())
            weekTypes.add(WeekType.SUNDAY);

        return weekTypes;
    }

    public void setWeek(List<WeekType> weekTypes) {
        if (weekTypes == null || weekTypes.size() == 0)
            setVisibility(GONE);
        else {
            disableAllViews();
            for (WeekType type : weekTypes)
                setDay(type);
        }
    }

    private void disableAllViews() {
        monday.setActivated(false);
        tuesday.setActivated(false);
        wednesday.setActivated(false);
        thursday.setActivated(false);
        friday.setActivated(false);
        saturday.setActivated(false);
        sunday.setActivated(false);
    }

    private void setDay(WeekType type) {
        switch (type) {

            case MONDAY:
                monday.setActivated(true);
                break;

            case TUESDAY:
                tuesday.setActivated(true);
                break;

            case WEDNESDAY:
                wednesday.setActivated(true);
                break;

            case THURSDAY:
                thursday.setActivated(true);
                break;

            case FRIDAY:
                friday.setActivated(true);
                break;

            case SATURDAY:
                saturday.setActivated(true);
                break;

            case SUNDAY:
                sunday.setActivated(true);
                break;

            default:
                break;
        }
    }

    public boolean isCorrectValues() {
        return getWeekTypes().size() > 0;
    }
}
