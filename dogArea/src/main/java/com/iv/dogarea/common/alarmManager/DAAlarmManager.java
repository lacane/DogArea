package com.iv.dogarea.common.alarmManager;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.iv.dogarea.common.eventsAlarmManager.OnAlarmReceiver;
import com.iv.dogarea.common.utils.DateTimeConvert;
import com.iv.dogarea.common.utils.EventsUtils;
import com.iv.dogarea.user.models.events.Event;

import org.joda.time.DateTime;

import java.util.Date;

public class DAAlarmManager {
    private AlarmManager alarmManager;
    private Context context;
    private PendingIntent pendingIntent;

    public DAAlarmManager(Context context) {
        this.context = context;
        alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
    }

    public void setEventAlarm(Event event, Intent intent) {
        Intent intentReceiver = new Intent(context, OnAlarmReceiver.class);
        intentReceiver.putExtras(intent.getExtras());

        pendingIntent = PendingIntent.getBroadcast(context, event.id(),
                        intentReceiver, PendingIntent.FLAG_UPDATE_CURRENT);

        EventsUtils utils = new EventsUtils(event);
        DateTime dateTime = utils.getNextDate();
        if (dateTime != null)
            setAlarm(dateTime.getMillis());
    }

    private void setAlarm(long time) {
        alarmManager.set(AlarmManager.RTC_WAKEUP,
                time,
                pendingIntent);

        Toast.makeText(context, "Ближайшее событие " +
                DateTimeConvert.getStringFullDate(new Date(time)), Toast.LENGTH_LONG).show();
    }
}
