package com.iv.dogarea.common.eventsAlarmManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.commonsware.cwac.wakeful.WakefulIntentService;

public class OnAlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent service = new Intent(context, NotificationsService.class);
        service.putExtras(intent.getExtras());
        WakefulIntentService.sendWakefulWork(context, service);
    }
}
