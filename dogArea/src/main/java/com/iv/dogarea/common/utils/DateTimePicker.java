package com.iv.dogarea.common.utils;


import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.view.View;

import org.joda.time.DateTime;

public class DateTimePicker {
    public static void startDateSimplePicker(Context context, DatePickerDialog.OnDateSetListener listener) {
        startDateSimplePicker(context, listener, null);
    }

    public static void startDateSimplePicker(Context context, DatePickerDialog.OnDateSetListener listener, View view) {
        DateTime date = new DateTime();
        DatePickerDialog pickerDialog = new DatePickerDialog(context, listener, date.getYear(),
                date.getMonthOfYear() - 1, date.getDayOfMonth());
        pickerDialog.show();

        if (view != null)
            DAUtils.hideKeyboard(context, view);
    }

        public static void startTimePicker(Context context, TimePickerDialog.OnTimeSetListener listener) {
        startTimePicker(context, listener, null);
    }

    public static void startTimePicker(Context context, TimePickerDialog.OnTimeSetListener listener, View view) {
        DateTime time = new DateTime();
        TimePickerDialog pickerDialog = new TimePickerDialog(context, listener, time.getHourOfDay(),
                time.getMinuteOfHour(), true);
        pickerDialog.show();

        if (view != null)
            DAUtils.hideKeyboard(context, view);
    }
}
