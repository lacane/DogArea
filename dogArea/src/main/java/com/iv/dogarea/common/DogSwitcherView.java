package com.iv.dogarea.common;


import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;

import com.iv.dogarea.R;
import com.iv.dogarea.common.utils.DAUtils;
import com.iv.dogarea.common.utils.DateTimeConvert;
import com.iv.dogarea.user.models.dogs.Dog;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DogSwitcherView extends FrameLayout {

    @BindView(R.id.profile_dog_image_switcher) ImageSwitcher imageSwitcher;
    @BindView(R.id.profile_dog_text_switcher) TextSwitcher textSwitcher;

    private List<Dog> dogs;
    private int currentDogItem;
    private ImageSwitcherPicasso switcherPicasso;
    private boolean enableCarousel;
    private int duration = 5000;
    private Animation nextIn;
    private Animation nextOut;


    public DogSwitcherView(Context context) {
        super(context);
        init();
    }

    public DogSwitcherView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DogSwitcherView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.view_dog_switcher, this);
        ButterKnife.bind(this);

        nextIn = AnimationUtils.loadAnimation(getContext(), R.anim.slide_left_in);
        nextOut = AnimationUtils.loadAnimation(getContext(), R.anim.slide_left_out);

        setupImageSwitcher();
        setupTextSwitcher();

    }

    public void setEnabled(boolean enabled) {
        enableCarousel = enabled;
    }

    private void setupTextSwitcher() {
        textSwitcher.setFactory(() -> {
            TextView textView = new TextView(getContext());
            textView.setLayoutParams(
                    new TextSwitcher.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT));
            textView.setLines(1);
            textView.setTypeface(Typeface.DEFAULT_BOLD);
            textView.setEllipsize(TextUtils.TruncateAt.END);
            textView.setGravity(Gravity.START | Gravity.CENTER_VERTICAL);
            textView.setTextSize(getResources().getDimensionPixelSize(R.dimen._5sdp));
            textView.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
            return textView;
        });

        Animation alphaOut = new AlphaAnimation(1, 0);
        Animation alphaIn = new AlphaAnimation(0, 1);
        alphaOut.setDuration(500);
        alphaIn.setDuration(500);

        textSwitcher.setInAnimation(alphaIn);
        textSwitcher.setOutAnimation(alphaOut);
    }

    private void setupImageSwitcher() {
        imageSwitcher.setFactory(() -> {
            ImageView image = new ImageView(getContext());
            image.setScaleType(ImageView.ScaleType.CENTER_CROP);
            image.setLayoutParams(new ImageSwitcher.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            return image;
        });

        imageSwitcher.setInAnimation(nextIn);
        imageSwitcher.setOutAnimation(nextOut);
    }

    public void setDogs(List<Dog> dogs) {
        this.dogs = dogs;

        enableCarousel = true;
        currentDogItem = 0;

        switcherPicasso = new ImageSwitcherPicasso(getContext(), imageSwitcher);

        if (dogs.size() > 0) {
            Dog dog = dogs.get(0);

            Picasso.with(getContext())
                    .load(dog.photo())
                    .placeholder(R.drawable.dog_placeholder_2)
                    .into(switcherPicasso);

            textSwitcher.setText(DAUtils.getNameAge(dog.name(),
                    DateTimeConvert.getAgeMonths(getContext(), dog.dateOfBirth())));

            startCarousel();
        }

    }

    private void startCarousel() {

        if (dogs.size() <= 1)
            return;

        imageSwitcher.postDelayed(() -> {
            currentDogItem++;

            if (currentDogItem >= dogs.size())
                currentDogItem = 0;

            Dog dog = dogs.get(currentDogItem);

            if (enableCarousel && dog.photo() != null && !"".equals(dog.photo())) {
                Picasso.with(getContext())
                        .load(dog.photo())
                        .placeholder(R.drawable.dog_placeholder_2)
                        .into(switcherPicasso);

                textSwitcher.setText(DAUtils.getNameAge(dog.name(),
                        DateTimeConvert.getAgeMonths(getContext(), dog.dateOfBirth())));
            }

            startCarousel();

        }, duration);

    }
}
