package com.iv.dogarea.common;


import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.iv.dogarea.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CircleView extends FrameLayout {

    private String textString;
    private String labelString;
    @BindView(R.id.view_time_text) TextView text;
    @BindView(R.id.view_time_label) TextView label;

    public CircleView(Context context) {
        super(context);
        init();
    }

    public CircleView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CircleView);
        labelString = a.getString(R.styleable.CircleView_label_circle);
        textString = a.getString(R.styleable.CircleView_text_circle);
        a.recycle();

        init();
    }

    public CircleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CircleView, defStyleAttr, 0);
        labelString = a.getString(R.styleable.CircleView_label_circle);
        textString = a.getString(R.styleable.CircleView_text_circle);
        a.recycle();

        init();
    }

    public void setText(String textString) {
        text.setText(textString);
    }

    public void setLabel(String labelString) {
        label.setText(labelString);
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.view_time, this);
        ButterKnife.bind(this);

        if (labelString != null)
            label.setText(labelString);

        if (textString != null)
            text.setText(textString);
    }
}
