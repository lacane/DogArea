package com.iv.dogarea.common;


import android.support.v7.widget.Toolbar;

public interface ToolbarProvider {
    Toolbar getToolbar();
}
