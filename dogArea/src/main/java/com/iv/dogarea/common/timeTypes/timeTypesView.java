package com.iv.dogarea.common.timeTypes;


import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;

import com.iv.dogarea.R;
import com.iv.dogarea.common.WeekView;
import com.iv.dogarea.common.dialog.TimeTypeDialog;
import com.iv.dogarea.common.utils.DateTimeConvert;
import com.iv.dogarea.common.utils.DateTimePicker;
import com.iv.dogarea.user.models.events.TimeType;
import com.iv.dogarea.user.models.events.WeekType;

import org.joda.time.DateTime;

import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TimeTypesView extends FrameLayout {

    @BindView(R.id.time_types_view_repeat) TimeTypeView repeat;
    @BindView(R.id.time_types_view_time) TimeTypeView time;
    @BindView(R.id.time_types_view_start_date) TimeTypeView startDate;
    @BindView(R.id.time_types_view_end_date) TimeTypeView endDate;
    @BindView(R.id.time_types_view_week) WeekView weekView;

    private int yearStart;
    private int monthStart;
    private int dayStart;

    private int yearEnd;
    private int monthEnd;
    private int dayEnd;

    private int hours;
    private int minutes;

    private TimeType timeType;

    private TimePickerDialog.OnTimeSetListener timeCallback = (timePicker, hourOfDay, minute) -> {
        hours = hourOfDay;
        minutes = minute;
        time.setText(DateTimeConvert.getTime(hourOfDay, minute));
    };
    private DatePickerDialog.OnDateSetListener startDateCallback = (datePicker, year, monthOfYear, dayOfMonth) -> {
        yearStart = year;
        monthStart = monthOfYear + 1;
        dayStart = dayOfMonth;
        startDate.setText(DateTimeConvert.getStringDateMonth(
                new DateTime(yearStart, monthStart, dayStart, 0, 0).toDate()));

    };
    private DatePickerDialog.OnDateSetListener endDateCallback = (datePicker, year, monthOfYear, dayOfMonth) -> {
        yearEnd = year;
        monthEnd = monthOfYear + 1;
        dayEnd = dayOfMonth;
        endDate.setText(DateTimeConvert.getStringDateMonth(
                new DateTime(yearEnd, monthEnd, dayEnd, 0, 0).toDate()));

    };

    public TimeTypesView(Context context) {
        super(context);
        init();
    }

    public TimeTypesView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TimeTypesView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.view_time_types, this);
        ButterKnife.bind(this);

        repeat.setOnClickListener(view -> {
            TimeTypeDialog dialog = new TimeTypeDialog(getContext(), this::changeTimeType);
            dialog.show();

        });
        time.setOnClickListener(view -> {
            DateTimePicker.startTimePicker(getContext(), timeCallback, view);
        });
        startDate.setOnClickListener(view -> {
            DateTimePicker.startDateSimplePicker(getContext(), startDateCallback, view);
        });
        endDate.setOnClickListener(view -> {
            DateTimePicker.startDateSimplePicker(getContext(), endDateCallback, view);
        });
        changeTimeType(TimeType.RANGE);

    }

    private void changeTimeType(TimeType type) {
        this.timeType = type;

        repeat.setText(type.getName(getContext()));

        switch (type) {
            case EVERY_WEEK:
                visibleEndDate(false, false, true);
                break;
            case EVERY_YEAR:
                visibleEndDate(false, true, false);
                break;
            case EVERYDAY:
                visibleEndDate(false, false, false);
                break;
            case ONE_TIME:
                visibleEndDate(false, true, false);
                break;
            case RANGE:
                visibleEndDate(true, true, false);
                break;
        }
    }

    private void visibleEndDate(boolean visibleEnd, boolean visibleStart, boolean visibleWeek) {
        weekView.setVisibility(visibleWeek ? VISIBLE : GONE);
        endDate.setVisibility(visibleEnd ? VISIBLE : GONE);
        startDate.setVisibility(visibleStart ? VISIBLE : GONE);
        startDate.setLabel(visibleEnd ?
                getContext().getString(R.string.start_date) : getContext().getString(R.string.date));
    }

    public TimeType getTimeType() {
        return timeType;
    }

    public List<WeekType> getWeekTypes() {
        return weekView.getWeekTypes();
    }

    public int getMinute() {
        return minutes;
    }

    public int getHour() {
        return hours;
    }

    public Date getEndDate() {
        if (yearEnd == 0 || monthEnd == 0 || dayEnd == 0)
            return null;
        return new DateTime(yearEnd, monthEnd, dayEnd, 0, 0).toDate();
    }

    public Date getStartDate() {
        if (yearStart == 0 || monthStart == 0 || dayStart == 0)
            return null;
        return new DateTime(yearStart, monthStart, dayStart, 0, 0).toDate();
    }

    public void setValues(TimeType timeType, List<WeekType> weekTypes, Date startDate,
                          Date endDate, Integer hour, Integer minute) {
        changeTimeType(timeType);
        this.weekView.setWeek(weekTypes);
        this.startDate.setText(DateTimeConvert.getStringDateMonth(startDate));
        this.endDate.setText(DateTimeConvert.getStringDateMonth(endDate));
        this.time.setText(DateTimeConvert.getTime(hour, minute));

        DateTime dateTime;

        if (startDate != null) {
            dateTime = new DateTime(startDate.getTime());
            yearStart = dateTime.getYear();
            monthStart = dateTime.getMonthOfYear();
            dayStart = dateTime.getDayOfMonth();
        }

        if (endDate != null) {
            dateTime = new DateTime(endDate.getTime());
            yearEnd = dateTime.getYear();
            monthEnd = dateTime.getMonthOfYear();
            dayEnd = dateTime.getDayOfMonth();
        }

        this.hours = hour;
        this.minutes = minute;
    }

    public boolean isCorrectValues() {

        switch (timeType) {

            case EVERY_WEEK:
                return weekView.isCorrectValues() && time.isCorrectValues();

            case ONE_TIME:
            case EVERY_YEAR:
                return time.isCorrectValues() && startDate.isCorrectValues();

            case EVERYDAY:
                return  time.isCorrectValues();

            case RANGE:
                return time.isCorrectValues() && startDate.isCorrectValues() && endDate.isCorrectValues();

            default:
                return false;
        }

    }
}
