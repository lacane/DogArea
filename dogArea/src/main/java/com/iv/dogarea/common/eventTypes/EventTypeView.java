package com.iv.dogarea.common.eventTypes;


import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatRadioButton;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.iv.dogarea.R;
import com.iv.dogarea.user.models.events.EventType;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EventTypeView extends FrameLayout {

    @BindView(R.id.event_type_view) LinearLayout root;
    @BindView(R.id.event_type_view_check) CheckBox checkBox;
    @BindView(R.id.event_type_view_radio) AppCompatRadioButton radioButton;

    private String titleString;
    private EventType type;

    public EventTypeView(Context context) {
        super(context);
        init();
    }

    public EventTypeView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.EventTypeView);
        titleString = a.getString(R.styleable.EventTypeView_text_event);
        a.recycle();

        init();
    }

    public EventTypeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.EventTypeView, defStyleAttr, 0);
        titleString = a.getString(R.styleable.EventTypeView_text_event);
        a.recycle();

        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.view_event_type, this);
        ButterKnife.bind(this);

        if (titleString != null) {
            checkBox.setText(titleString);
            radioButton.setText(titleString);
        }
    }

    public void setOnClickListener(OnClickListener listener) {
        root.setOnClickListener(view -> {
            setChecked(true);
            listener.onClick(this);
        });
    }

    public void setChecked(boolean checked) {
        checkBox.setChecked(checked);
        radioButton.setChecked(checked);
    }

    public boolean isChecked() {
        return checkBox.isChecked();
    }

    public EventType getType() {
        return type;
    }

    public void setType(EventType type, boolean oneChecked) {
        this.type = type;

        checkBox.setText(type.getName(getContext()));
        radioButton.setText(type.getName(getContext()));

        setOneChecked(oneChecked);
    }

    public void setOneChecked(boolean oneChecked) {
        if (oneChecked) {
            checkBox.setVisibility(GONE);
            radioButton.setVisibility(VISIBLE);
        } else {
            checkBox.setVisibility(VISIBLE);
            radioButton.setVisibility(GONE);
        }
    }
}
