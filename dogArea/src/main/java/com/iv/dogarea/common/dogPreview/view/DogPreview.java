package com.iv.dogarea.common.dogPreview.view;


import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.iv.dogarea.R;
import com.iv.dogarea.user.models.dogs.Dog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DogPreview extends FrameLayout {

    @BindView(R.id.dog_preview_list) LinearLayout listContainer;

    private OnClickDogItemListener listener;
    private List<Dog> dogs;

    public DogPreview(Context context) {
        super(context);
        init();
    }
    public DogPreview(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
    public DogPreview(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.view_dog_preview, this);
        ButterKnife.bind(this);
        dogs = new ArrayList<>();
    }

    public void setupDogsData(List<Dog> dogs) {
        this.dogs = dogs;
        listContainer.removeAllViews();
        for (int index = 0; index < dogs.size(); index++) {
            Dog dog = dogs.get(index);
            DogItem item = new DogItem(getContext());
            item.setOnClickDogItemListener(listener);
            item.setDogModel(dog);
            item.setIndex(index);
            listContainer.addView(item);
        }
    }

    public void setOnClickDogItemListener(OnClickDogItemListener lister) {
        this.listener = lister;
    }

    public boolean isCorrectValue() {
        return dogs.size() != 0;
    }

    public interface OnClickDogItemListener {
        void onClick(int index);
    }
}
