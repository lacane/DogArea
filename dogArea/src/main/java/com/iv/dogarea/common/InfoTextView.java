package com.iv.dogarea.common;


import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.iv.dogarea.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InfoTextView extends FrameLayout {

    private String titleString;
    @BindView(R.id.info_text_view_label) TextView label;
    @BindView(R.id.info_text_view_text) TextView text;

    public InfoTextView(Context context) {
        super(context);
        init(context);
    }

    public InfoTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.InfoTextView);

        titleString = a.getString(R.styleable.InfoTextView_title_info);

        a.recycle();

        init(context);
    }

    public InfoTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.InfoTextView, defStyleAttr, 0);

        titleString = a.getString(R.styleable.InfoTextView_title_info);
        a.recycle();

        init(context);
    }

    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.view_info_text, this);
        ButterKnife.bind(this);

        if (titleString != null)
            setLabel(titleString);
    }

    public void setLabel(String label) {
        this.label.setText(label);
    }

    public void setText(String text) {
        if (text != null && !text.equals(""))
            this.text.setText(text);
        else
            this.text.setText(getContext().getString(R.string.unknown));
    }
}
