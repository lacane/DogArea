package com.iv.dogarea.common.utils;


import com.iv.dogarea.user.models.events.Event;
import com.iv.dogarea.user.models.events.TimeType;
import com.iv.dogarea.user.models.events.WeekType;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("ConstantConditions")
public class EventsUtils {

    private Event event;

    public EventsUtils(Event event) {
        this.event = event;
    }

    private DateTime getTimeAlarm() {
        DateTime dateTime = DateTime.now();
        dateTime = dateTime.withHourOfDay(event.hour()).withMinuteOfHour(event.minute());

        if (dateTime.isBeforeNow())
            dateTime = dateTime.plusDays(1);

        return dateTime;
    }

    private DateTime getDateRangeAlarm() {
        boolean needAlarm = false;
        DateTime dateTime = DateTime.now();


        if (dateTime.isBefore(event.startDate().getTime())) {
            needAlarm = true;
            dateTime = new DateTime(event.startDate().getTime())
                    .plusHours(event.hour())
                    .plusMinutes(event.minute());
        } else {
            DateTime endDate = new DateTime(event.endDate().getTime())
                    .plusHours(event.hour())
                    .plusMinutes(event.minute());

            if (!dateTime.isAfter(endDate.getMillis())) {
                needAlarm = true;
                dateTime = dateTime.withHourOfDay(event.hour()).withMinuteOfHour(event.minute());

                if (dateTime.isBefore(DateTime.now().getMillis())) {
                    dateTime = dateTime.plusDays(1);
                    if (dateTime.isAfter(endDate.getMillis()))
                        needAlarm = false;
                }
            }
        }

        if (needAlarm)
            return dateTime;
        else
            return null;
    }

    private DateTime getDateTimeAlarm() {
        boolean needAlarm = false;
        DateTime dateTime = new DateTime(event.startDate().getTime());
        dateTime = dateTime.plusHours(event.hour()).plusMinutes(event.minute());

        if (dateTime.isBeforeNow()) {
            if (event.timeType() == TimeType.EVERY_YEAR) {
                dateTime = dateTime.plusYears(1);
                needAlarm = true;
            }
        } else
            needAlarm = true;

        if (needAlarm)
            return dateTime;
        else
            return null;
    }

    private DateTime getWeekAlarm() {
        DateTime dateTime = DateTime.now();

        int offset = -1;
        boolean exit = event.weekTypes().size() > 0;

        List<WeekType> weekTypes = new ArrayList<>();
        weekTypes.addAll(event.weekTypes());

        // create new array with events
        int currentDay = dateTime.getDayOfWeek();
        for (int index = 0; index < event.weekTypes().size(); index++) {
            if (event.weekTypes().get(index).getDay() < currentDay) {
                int indexForRemove = weekTypes.indexOf(event.weekTypes().get(index));
                weekTypes.remove(indexForRemove);
                weekTypes.add(event.weekTypes().get(index));
            }
        }

        //determinate offset for the day of a week
        for (int index = 0; exit; index++) {

            WeekType weekType = weekTypes.get(index);

            if (weekType.getDay().equals(currentDay)) {
                exit = false;

                if (checkTime(dateTime, event.hour(), event.minute()))
                    offset = 0;
                else if (index == weekTypes.size() - 1)
                    offset = 7 + weekTypes.get(0).getDay() - currentDay;
                else
                    exit = true;

            } else if (weekType.getDay() > currentDay) {
                exit = false;
                offset = weekType.getDay() - currentDay;
            }
            else if (index == weekTypes.size() - 1)
                offset = 7 + event.weekTypes().get(0).getDay() - currentDay;

            if (exit)
                exit = weekTypes.size() > index + 1;
        }

        dateTime = dateTime.withHourOfDay(event.hour()).withMinuteOfHour(event.minute());

        if (offset != -1) {
            dateTime = dateTime.plusDays(offset);
        }

        return dateTime;
    }

    private boolean checkTime(DateTime dateTime, Integer hour, Integer minute) {
        int compareHour = hour.compareTo(dateTime.getHourOfDay());

        if (compareHour == -1)
            return false;
        else if (compareHour == 0) {
            int compareMinute = minute.compareTo(dateTime.getMinuteOfHour());

            if (compareMinute <= 0)
                return false;
        }

        return true;
    }

    public DateTime getNextDate() {
        switch (event.timeType()) {
            case EVERY_WEEK:
                return getWeekAlarm();

            case EVERY_YEAR:
                return getDateTimeAlarm();

            case EVERYDAY:
                return getTimeAlarm();

            case ONE_TIME:
                return getDateTimeAlarm();

            case RANGE:
                return getDateRangeAlarm();

            default:
                return null;
        }
    }
}
