package com.iv.dogarea.common.dialog;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.iv.dogarea.R;
import com.iv.dogarea.user.models.events.TimeType;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TimeTypeDialog extends Dialog {

    @BindView(R.id.dialog_time_one) TextView one;
    @BindView(R.id.dialog_time_everyday) TextView everyday;
    @BindView(R.id.dialog_time_every_week) TextView everyWeek;
    @BindView(R.id.dialog_time_every_year) TextView everyYear;
    @BindView(R.id.dialog_time_range) TextView range;

    private OnSelectTimeType selectTimeType;
    private View.OnClickListener listener = view -> {
        switch (view.getId()) {

            case R.id.dialog_time_one:
                selectTimeType.onSelect(TimeType.ONE_TIME);
                break;

            case R.id.dialog_time_everyday:
                selectTimeType.onSelect(TimeType.EVERYDAY);
                break;

            case R.id.dialog_time_every_week:
                selectTimeType.onSelect(TimeType.EVERY_WEEK);
                break;

            case R.id.dialog_time_every_year:
                selectTimeType.onSelect(TimeType.EVERY_YEAR);
                break;

            case R.id.dialog_time_range:
                selectTimeType.onSelect(TimeType.RANGE);
                break;

            default:
                selectTimeType.onSelect(TimeType.ONE_TIME);
                break;
        }

        dismiss();
    };

    public TimeTypeDialog(Context context, OnSelectTimeType selectTimeType) {
        super(context);
        this.selectTimeType = selectTimeType;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_time_type);
        ButterKnife.bind(this);

        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        one.setOnClickListener(listener);
        everyday.setOnClickListener(listener);
        everyWeek.setOnClickListener(listener);
        everyYear.setOnClickListener(listener);
        range.setOnClickListener(listener);
    }

    public interface OnSelectTimeType {
        void onSelect(TimeType type);
    }
}
