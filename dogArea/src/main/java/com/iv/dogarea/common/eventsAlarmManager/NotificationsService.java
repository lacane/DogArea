package com.iv.dogarea.common.eventsAlarmManager;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.commonsware.cwac.wakeful.WakefulIntentService;
import com.iv.dogarea.R;
import com.iv.dogarea.common.alarmManager.DAAlarmManager;
import com.iv.dogarea.common.storage.StorageServiceConst;
import com.iv.dogarea.dogareainc.alarm.view.AlarmActivity;
import com.iv.dogarea.user.models.events.Event;

public class NotificationsService extends WakefulIntentService implements StorageServiceConst {
    public NotificationsService() {
        super("NotificationsService");
    }

    private int idOfNotification;

    @Override
    protected void doWakefulWork(Intent intent) {

        idOfNotification = 0;

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_notification)
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(),
                                R.mipmap.ic_launcher))
                        .setAutoCancel(true);

        mBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
        mBuilder.setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE);

        String type = intent.getStringExtra(NOTIFICATION_TYPE);
        if (type == null)
            return;

        switch (type) {
            case EVENT:
                showEventNotification(intent, mBuilder);
                break;
        }

        checkApiBuild(mBuilder);

        Intent resultIntent = new Intent(this, AlarmActivity.class);
        resultIntent.putExtras(intent.getExtras());

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(AlarmActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);

        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(idOfNotification, mBuilder.build());
    }

    private void showEventNotification(Intent intent, NotificationCompat.Builder mBuilder) {
        Event event = (Event) intent.getSerializableExtra(EVENT);

        if (event != null) {
            String title = event.name().getName(this);

            title += " (" + event.nameDog() + ")";

            mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(event.description()));
            mBuilder.setContentTitle(title);
            mBuilder.setContentText(event.description());
            mBuilder.setTicker(event.name().getName(this));

            idOfNotification = event.id();

            DAAlarmManager manager = new DAAlarmManager(this);
            manager.setEventAlarm(event, intent);
        }
    }

    private void checkApiBuild(NotificationCompat.Builder builder) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setVisibility(Notification.VISIBILITY_PUBLIC)
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setVibrate(new long[0]);
        }
    }

}
