package com.iv.dogarea;


import android.support.multidex.MultiDexApplication;

import com.iv.dogarea.di.HasComponent;
import com.iv.dogarea.di.components.ApplicationComponent;

public abstract class DogAreaApplication extends MultiDexApplication
        implements HasComponent<ApplicationComponent> {
    protected abstract void setupDagger();
}