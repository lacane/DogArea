package com.iv.dogarea.dogareainc.welcome.auth.presenter;


import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.util.Log;

import com.alapshin.arctor.presenter.PresenterBundle;
import com.alapshin.arctor.presenter.rx.RxPresenter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.iv.dogarea.dogareainc.welcome.auth.view.AuthView;

import javax.annotation.Nullable;

public class AuthPresenterImpl extends RxPresenter<AuthView> implements AuthPresenter,
        FirebaseAuth.AuthStateListener, OnCompleteListener<AuthResult> {

    private int currentPosition;
    private FirebaseAuth firebaseAuth;

    public AuthPresenterImpl() {
        this.currentPosition = 0;
    }

    @Override
    public void onCreate(@Nullable PresenterBundle bundle) {
        super.onCreate(bundle);
        firebaseAuth = FirebaseAuth.getInstance();
    }

    @Override
    public void onStart() {
        super.onStart();
        firebaseAuth.addAuthStateListener(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        firebaseAuth.removeAuthStateListener(this);
    }

    @Override
    public void onComplete(@NonNull Task<AuthResult> task) {
        Log.w("AuthFragment", "signInWithCustomToken:onComplete:" + task.isSuccessful());

        // If sign in fails, display a message to the user. If sign in succeeds
        // the auth state listener will be notified and logic to handle the
        // signed in user can be handled in the listener.

        getView().successAuth();

        if (!task.isSuccessful()) {
            getView().authWrong();
            Log.w("AuthFragment", "signInWithCustomToken", task.getException());
        }


    }

    @Override
    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (user != null) {
            // User is signed in
            Log.d("AuthFragment", "onAuthStateChanged:signed_in:" + user.getUid());
        } else {
            // User is signed out
            Log.d("AuthFragment", "onAuthStateChanged:signed_out");
        }

    }

    @Override
    public FirebaseAuth.AuthStateListener getAuthListener() {
        return this;
    }

    @Override
    public OnCompleteListener<AuthResult> getCompleteListener() {
        return this;
    }

    @Override
    public TabLayout.OnTabSelectedListener getTabListener() {
        return new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 1 && currentPosition != 1) {
                    currentPosition = 1;
                    getView().nextPage();

                } else if (tab.getPosition() == 0 && currentPosition != 0) {
                    currentPosition = 0;
                    getView().previousPage();
                }
            }
            @Override public void onTabUnselected(TabLayout.Tab tab) {}
            @Override public void onTabReselected(TabLayout.Tab tab) {}
        };
    }

    @Override
    public void login(String email, String password) {
        if (checkEmail(email)) {
            if (checkPassword(password)) {
                firebaseAuth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(this);
            } else
                getView().passwordWrong();
        } else
            getView().emailWrong();
    }

    @Override
    public void signUp(String email, String password, String passwordAgain) {
        if (checkEmail(email)) {
            if (checkPassword(password)) {
                if (password.equals(passwordAgain)) {
                    firebaseAuth.createUserWithEmailAndPassword(email, password)
                            .addOnCompleteListener(this);
                } else
                    getView().passwordAgainWrong();
            } else
                getView().passwordWrong();
        } else
            getView().emailWrong();
    }

    private boolean checkPassword(String password) {
        return password.length() >= 4;
    }

    private boolean checkEmail(String email) {
        return email.length() > 3 && email.contains("@") && email.contains(".");
    }
}
