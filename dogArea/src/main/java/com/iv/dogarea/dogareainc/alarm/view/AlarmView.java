package com.iv.dogarea.dogareainc.alarm.view;


import com.alapshin.arctor.view.MvpView;
import com.iv.dogarea.user.models.dogs.Dog;
import com.iv.dogarea.user.models.events.Event;

import java.util.List;

public interface AlarmView extends MvpView {
    void setEventData(Event event);
    void setDogsData(List<Dog> dogs);
}
