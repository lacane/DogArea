package com.iv.dogarea.dogareainc.navigation.view;


import android.content.Context;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;

public class FBAnalytics {
    private FirebaseAnalytics firebaseAnalytics;

    public FBAnalytics(Context context) {
        firebaseAnalytics = FirebaseAnalytics.getInstance(context);
    }

    public void sendShareEvent() {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "share");
        bundle.putString(FirebaseAnalytics.Param.ACHIEVEMENT_ID, "achievement share");
        bundle.putString(FirebaseAnalytics.Param.CHARACTER, "CHARACTER share");
        bundle.putString(FirebaseAnalytics.Param.VALUE, "value share");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SHARE, bundle);
    }
}
