package com.iv.dogarea.dogareainc.activity.models;

import android.content.Context;

import com.iv.dogarea.R;

enum ActivityType {
    EVENT;

    public String getName(Context context) {
        switch (this) {
            case EVENT:
                return context.getString(R.string.event);

            default:
                return null;
        }
    }
}