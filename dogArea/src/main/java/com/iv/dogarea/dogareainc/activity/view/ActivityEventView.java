package com.iv.dogarea.dogareainc.activity.view;


import android.content.Context;
import android.widget.FrameLayout;

import com.iv.dogarea.dogareainc.activity.models.ActivityItem;
import com.iv.dogarea.user.models.events.Event;

import java.util.Date;

public class ActivityEventView extends FrameLayout implements ActivityItem {
    public ActivityEventView(Context context) {
        super(context);
    }

    private void init() {

    }

    public ActivityEventView setEvent(Event event) {
        return this;
    }

    @Override
    public Date getDate() {
        return null;
    }
}
