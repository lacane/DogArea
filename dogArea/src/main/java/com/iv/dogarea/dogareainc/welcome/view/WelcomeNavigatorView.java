package com.iv.dogarea.dogareainc.welcome.view;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.iv.dogarea.R;
import com.iv.dogarea.common.ToolbarProvider;
import com.iv.dogarea.dogareainc.welcome.auth.view.AuthFragment;
import com.iv.dogarea.dogareainc.welcome.models.WNType;
import com.iv.dogarea.dogareainc.welcome.splash.view.SplashFragment;

public class WelcomeNavigatorView {

    private Context context;
    private FragmentManager fragmentManager;

    public WelcomeNavigatorView(Context context, FragmentManager fragmentManager) {
        this.context = context;
        this.fragmentManager = fragmentManager;
    }

    void goToScreen(WNType type) {
        goToScreen(type, true);
    }

    public void goToScreen(WNType type, boolean animation) {
        switch (type) {
            case AUTH:
                ((ToolbarProvider) context).getToolbar().setTitle(context.getString(R.string.app_name));
                startFragment(new AuthFragment(), animation);
                break;
            case SPLASH:
                startFragment(new SplashFragment(), animation);
                break;
        }
    }

    private void startFragment(Fragment fragment, boolean animation) {
        fragmentManager.beginTransaction()
                .replace(R.id.welcome_container, fragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
    }
}
