package com.iv.dogarea.dogareainc.edit.editDog.view;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.dogarea.base.mvp.view.BaseMvpActivity;
import com.iv.dogarea.R;
import com.iv.dogarea.common.GenderSwitchView;
import com.iv.dogarea.common.utils.DAUtils;
import com.iv.dogarea.common.utils.DateTimeConvert;
import com.iv.dogarea.common.utils.ImageUtils;
import com.iv.dogarea.di.HasComponent;
import com.iv.dogarea.di.components.ApplicationComponent;
import com.iv.dogarea.di.components.EditComponent;
import com.iv.dogarea.dogareainc.edit.editDog.presenter.EditDogPresenter;
import com.iv.dogarea.user.models.dogs.Dog;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;

import java.util.Date;
import java.util.UUID;

import butterknife.BindView;

public class EditDogActivity extends BaseMvpActivity<EditDogView, EditDogPresenter>
        implements EditDogView {

    public static String INDEX_OF_DOG = "index_of_dog";

    private EditComponent component;

    private Dog dog;
    private String photoPath;

    @BindView(R.id.dog_edit_image) ImageView dogImage;
    @BindView(R.id.dog_edit_name) EditText dogName;
    @BindView(R.id.dog_edit_race) EditText dogRace;
    @BindView(R.id.dog_edit_dob) MaterialEditText dogDob;
    @BindView(R.id.dog_edit_info) EditText dogInfo;
    @BindView(R.id.dog_edit_gender) GenderSwitchView genderSwitch;
    @BindView(R.id.dog_edit_save_button) Button save;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_edit_dog;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void injectDependencies() {
        if (component == null)
            component = ((HasComponent<ApplicationComponent>) getApplication()).getComponent().editComponent();

        component.inject(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DAUtils.hideKeyboard(this, dogName);

        dogImage.setOnClickListener(view1 -> ImageUtils.openChooser(this));
        presenter.setTextWatcher(dogName, dogRace, dogDob, dogInfo);

        dogDob.setOnClickListener(view1 -> presenter.startDataPicker(this));
        save.setOnClickListener(view -> presenter.storeDogData(getNewDogData()));

        presenter.setData(getIntent());
    }

    private Dog getNewDogData() {
        this.dog = Dog.builder(dog)
                .gender(genderSwitch.getState())
                .info(dogInfo.getText().toString())
                .name(dogName.getText().toString())
                .race(dogRace.getText().toString())
                .photo(photoPath)
                .build();

        return dog;
    }

    @Override
    public void getDogDataForEdit(Dog dog) {
        this.dog = dog;

        setDogData(dog);
    }

    private void setDogData(Dog dog) {
        setValue(dogName, dog.name());
        setValue(dogRace, dog.race());
        setValue(dogInfo, dog.info());
        dogDob.setText(DateTimeConvert.getStringDate(dog.dateOfBirth()));
        genderSwitch.setState(dog.gender());
        genderSwitch.setChangeStateListener(gender -> checkTexts());

        photoPath = dog.photo();
        if (photoPath != null) {
            Picasso.with(this)
                    .load(Uri.parse(photoPath))
                    .placeholder(R.drawable.dog_placeholder_2)
                    .centerCrop()
                    .fit()
                    .into(dogImage);
        }

    }

    private void setValue(EditText editText, String s) {
        if (s != null) editText.setText(s);
    }


    @Override
    public void selectedDate(Date selectedDate) {
        dog = Dog.builder(dog).dateOfBirth(selectedDate).build();
        dogDob.setText(DateTimeConvert.getStringDate(selectedDate));
    }

    @Override
    public void changeText() {
        checkTexts();
    }

    private void checkTexts() {
        boolean enable;

        enable = dogName.getText().toString().length() > 0
                && dogRace.getText().toString().length() > 0
                && genderSwitch.isChecked();

        save.setEnabled(enable);
    }

    @Override
    public void completeSaving() {
        finish();
    }

    @Override
    public void addingDog() {
        genderSwitch.setChangeStateListener(gender -> checkTexts());
        dog = Dog.builder().id(UUID.randomUUID().toString()).build();
        save.setText(getString(R.string.add_dog));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null && requestCode == UCrop.REQUEST_CROP) {
            checkTexts();
            Uri uri = UCrop.getOutput(data);
            photoPath = String.valueOf(uri);

            Picasso.with(this)
                    .load(uri)
                    .placeholder(R.drawable.placeholder)
                    .centerCrop()
                    .fit()
                    .into(dogImage);
        } else {
            ImageUtils.handleActivityResult(requestCode, resultCode, data, this, false);
        }

    }
}
