package com.iv.dogarea.dogareainc.welcome.models;


import com.iv.dogarea.dogareainc.welcome.view.WelcomeNavigatorView;

public interface WelcomeNavigator {
    WelcomeNavigatorView getNavigator();
}
