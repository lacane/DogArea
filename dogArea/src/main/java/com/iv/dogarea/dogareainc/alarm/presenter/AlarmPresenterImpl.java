package com.iv.dogarea.dogareainc.alarm.presenter;


import android.content.Intent;

import com.alapshin.arctor.presenter.rx.RxPresenter;
import com.iv.dogarea.common.storage.StorageServiceConst;
import com.iv.dogarea.dogareainc.alarm.view.AlarmView;
import com.iv.dogarea.user.models.dogs.DogsModel;
import com.iv.dogarea.user.models.events.Event;

import rx.Subscription;

public class AlarmPresenterImpl extends RxPresenter<AlarmView> implements AlarmPresenter, StorageServiceConst {
    private final DogsModel dogsModel;

    public AlarmPresenterImpl(DogsModel dogsModel) {
        this.dogsModel = dogsModel;
    }

    @Override
    public void getDogsData() {
        Subscription dataSubscription = dogsModel.getDogsData().subscribe(dogs -> {
            getView().setDogsData(dogs);
        });

        addSubscription(dataSubscription);
    }

    @Override
    public void setData(Intent data) {
        if (data != null) {
            Event event = (Event) data.getSerializableExtra(EVENT);
            if (event != null)
                getView().setEventData(event);
        }
    }
}
