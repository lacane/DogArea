package com.iv.dogarea.dogareainc.navigation.models;


import com.iv.dogarea.dogareainc.navigation.view.DrawNavView;
import com.iv.dogarea.user.models.User;

public interface DrawerNavigator {
    DrawNavView getNavigator();
    void openDrawer();
    void updateUser(User user);
}
