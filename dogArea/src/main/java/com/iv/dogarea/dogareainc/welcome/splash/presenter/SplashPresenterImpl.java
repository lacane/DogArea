package com.iv.dogarea.dogareainc.welcome.splash.presenter;

import com.alapshin.arctor.presenter.rx.RxPresenter;
import com.iv.dogarea.common.storage.StorageService;
import com.iv.dogarea.dogareainc.welcome.splash.view.SplashView;
import com.iv.dogarea.user.models.User;
import com.iv.dogarea.user.models.UserModel;
import com.iv.dogarea.user.models.dogs.Dog;
import com.iv.dogarea.user.models.dogs.DogsModel;
import com.iv.dogarea.user.models.events.Event;
import com.iv.dogarea.user.models.events.EventsModel;

import java.util.List;

import rx.Subscription;

public class SplashPresenterImpl extends RxPresenter<SplashView> implements SplashPresenter {
    private final UserModel userModel;
    private final EventsModel eventsModel;
    private final DogsModel dogsModel;

    public SplashPresenterImpl(UserModel userModel, EventsModel eventsModel, DogsModel dogsModel) {
        this.userModel = userModel;
        this.eventsModel = eventsModel;
        this.dogsModel = dogsModel;
    }

    @Override
    public void setupData() {
        setupUserData();
        setupEvents();
        setupDogs();
    }

    @Override
    public void setupUserData() {
        User user = StorageService.getUser();
        Subscription dataSubscription = userModel.setUser(user).subscribe(user1 -> {
            getView().completeUserData(user1);
        });

        addSubscription(dataSubscription);
    }

    @Override
    public void setupEvents() {
        List<Event> events = StorageService.getEvents();
        Subscription dataSubscription = eventsModel.setEvents(events).subscribe(events1 -> {
            getView().completeEvents(events1);
        });

        addSubscription(dataSubscription);
    }

    @Override
    public void setupDogs() {
        List<Dog> dogs = StorageService.getDogs();
        Subscription dataSubscription = dogsModel.setDogsData(dogs).subscribe(dogs1 -> {
            getView().completeDogs(dogs1);
        });

        addSubscription(dataSubscription);
    }
}
