package com.iv.dogarea.dogareainc.events.widget;


import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iv.dogarea.R;
import com.iv.dogarea.common.CircleView;
import com.iv.dogarea.common.DateView;
import com.iv.dogarea.common.WeekView;
import com.iv.dogarea.common.utils.DateTimeConvert;
import com.iv.dogarea.user.models.events.Event;
import com.iv.dogarea.user.models.events.EventNotificationType;
import com.iv.dogarea.user.models.events.TimeType;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EventItemView extends FrameLayout {

    @BindView(R.id.item_events_time_repeat) TextView timeRepeat;
    @BindView(R.id.item_events_name) TextView name;
    @BindView(R.id.item_events_background) LinearLayout backgroundView;
    @BindView(R.id.item_events_description) TextView description;
    @BindView(R.id.item_events_notification_type) TextView notificationType;
    @BindView(R.id.item_events_week) WeekView weekView;
    @BindView(R.id.item_events_start_date) DateView startDate;
    @BindView(R.id.item_events_end_date) DateView endDate;
    @BindView(R.id.item_events_time) CircleView timeView;

    private Event event;
    private EventsRecyclerAdapter.OnClickEvent clickEvent;

    public EventItemView(Context context) {
        super(context);
        init();
    }

    public EventItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EventItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.item_events, this);
        ButterKnife.bind(this);
    }

    private void setName(String name) {
        this.name.setText(name);
    }

    public void setTime(TimeType type) {
        timeRepeat.setText(type.getName(getContext()));

        switch (type) {
            case ONE_TIME:
                visibleDates(true, false, false);
                break;

            case EVERYDAY:
                visibleDates(false, false, false);
                break;

            case EVERY_YEAR:
                visibleDates(true, false, false);
                break;

            case EVERY_WEEK:
                visibleDates(false, false, true);
                break;

            case RANGE:
                visibleDates(true, true, false);
                break;

            default:
                break;
        }
    }

    private void visibleDates(boolean startDateVisible, boolean endDateVisible, boolean weekVisible) {
        startDate.setVisibility(startDateVisible ? VISIBLE : GONE);
        endDate.setVisibility(endDateVisible ? VISIBLE : GONE);
        weekView.setVisibility(weekVisible ? VISIBLE : GONE);
    }

    private void setDescription(String descriptionString) {
        if (descriptionString != null) {
            description.setText(descriptionString);
        } else
            description.setVisibility(GONE);
    }

    private void setNotificationType(EventNotificationType type) {
        notificationType.setText(type.getName(getContext()));
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public void bind() {
        if (event != null) {
            setDescription(event.description());
            setNotificationType(event.eventNotificationType());
            setTime(event.timeType());
            timeView.setText(DateTimeConvert.getTime(event.hour(), event.minute()));
            startDate.setDate(event.startDate());
            endDate.setDate(event.endDate());
            weekView.setWeek(event.weekTypes());

            String title = event.name().getName(getContext());
            title += " (" + event.nameDog() + ")";

            name.setText(title);
        }
    }

    public void setClickEvent(EventsRecyclerAdapter.OnClickEvent clickEvent) {
        this.clickEvent = clickEvent;

        backgroundView.setOnClickListener(view -> {
            clickEvent.onClick(event.id());
        });
    }
}
