package com.iv.dogarea.dogareainc.alarm.presenter;


import android.content.Intent;

import com.alapshin.arctor.presenter.Presenter;
import com.iv.dogarea.dogareainc.alarm.view.AlarmView;

public interface AlarmPresenter extends Presenter<AlarmView> {
    void getDogsData();
    void setData(Intent intent);
}
