package com.iv.dogarea.dogareainc.welcome.auth.presenter;


import android.support.design.widget.TabLayout;

import com.alapshin.arctor.presenter.Presenter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.iv.dogarea.dogareainc.welcome.auth.view.AuthView;

public interface AuthPresenter extends Presenter<AuthView>  {
    FirebaseAuth.AuthStateListener getAuthListener();
    OnCompleteListener<AuthResult> getCompleteListener();

    TabLayout.OnTabSelectedListener getTabListener();

    void login(String email, String password);
    void signUp(String email, String password, String passwordAgain);
}
