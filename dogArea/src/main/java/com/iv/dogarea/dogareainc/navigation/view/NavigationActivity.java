package com.iv.dogarea.dogareainc.navigation.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.dogarea.base.BaseFontActivity;
import com.iv.dogarea.R;
import com.iv.dogarea.common.ToolbarProvider;
import com.iv.dogarea.common.utils.DateTimeConvert;
import com.iv.dogarea.di.HasComponent;
import com.iv.dogarea.di.components.ApplicationComponent;
import com.iv.dogarea.di.components.NavigationComponent;
import com.iv.dogarea.dogareainc.navigation.models.DNType;
import com.iv.dogarea.dogareainc.navigation.models.DrawerNavigator;
import com.iv.dogarea.dogareainc.navigation.models.FirebaseManager;
import com.iv.dogarea.user.models.User;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class NavigationActivity extends BaseFontActivity
        implements HasComponent<NavigationComponent>, ToolbarProvider, DrawerNavigator, FirebaseManager {

    public static String CORRECT_VALUES = "correctValues";

    @BindView(R.id.navigation_toolbar) Toolbar toolbar;
    @BindView(R.id.navigation_view) NavigationView navigationView;
    @BindView(R.id.navigation_drawer_layout) DrawerLayout drawer;

    private CircleImageView userImage;
    private TextView userName;
    private TextView userAge;

    private NavigationComponent component;
    private DrawNavView navigator;
    private FBAnalytics analytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        setSupportActionBar(toolbar);

        navigator = new DrawNavView(this, getSupportFragmentManager());
        navigator.setDrawer(drawer);

        navigationView.setNavigationItemSelectedListener(navigator.getChangeScreen());

        setupHeaderViews();

        checkCorrectValues(getIntent());
    }

    private void checkCorrectValues(Intent intent) {
        if (intent == null)
            navigator.goToScreen(DNType.ACTIVITY);
        else {
            boolean correctValues = intent.getBooleanExtra(CORRECT_VALUES, false);

            if (correctValues)
                navigator.goToScreen(DNType.ACTIVITY);
            else
                navigator.goToScreen(DNType.PROFILE);
        }
    }

    private void setupHeaderViews() {
        NavigationView navigationView = (NavigationView) drawer.findViewById(R.id.navigation_view);
        View header = navigationView.getHeaderView(0);

        userImage = (CircleImageView) header.findViewById(R.id.drawer_image);
        userName = (TextView) header.findViewById(R.id.drawer_name);
        userAge = (TextView) header.findViewById(R.id.drawer_age);
    }

    @Override
    public void onBackPressed() {
        if (navigator.isDrawerClose())
            super.onBackPressed();
    }

    @Override
    @SuppressWarnings("unchecked")
    public NavigationComponent getComponent() {
        if (component == null)
            component = ((HasComponent<ApplicationComponent>) getApplication())
                    .getComponent().navigationComponent();

        return component;
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }

    @Override
    public void openDrawer() {
        drawer.openDrawer(GravityCompat.START);
    }

    @Override
    public void updateUser(User user) {
        if (user.photo() != null)
            Picasso.with(this).load(user.photo()).into(userImage);

        if (user.username() != null)
            userName.setText(user.username());

        if (user.dateOfBirth() != null)
            userAge.setText(DateTimeConvert.getAge(this, user.dateOfBirth()));
    }

    @Override
    public DrawNavView getNavigator() {
        return navigator;
    }

    @Override
    public FBAnalytics getAnalytics() {
        if (analytics == null)
            analytics = new FBAnalytics(this);

        return analytics;
    }
}
