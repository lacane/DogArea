package com.iv.dogarea.dogareainc.edit.editUser.view;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.dogarea.base.mvp.view.BaseMvpFragment;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.iv.dogarea.R;
import com.iv.dogarea.common.dialog.InfoDialog;
import com.iv.dogarea.common.utils.DAUtils;
import com.iv.dogarea.common.utils.DateTimeConvert;
import com.iv.dogarea.common.utils.ImageUtils;
import com.iv.dogarea.di.HasComponent;
import com.iv.dogarea.di.components.EditComponent;
import com.iv.dogarea.dogareainc.edit.editUser.presenter.EditUserPresenter;
import com.iv.dogarea.user.models.User;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;

import java.util.Date;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;

public class EditUserFragment extends BaseMvpFragment<EditUserView, EditUserPresenter>
        implements EditUserView {

    private final int AUTOCOMPLETE_REQUEST = 123;

    @BindView(R.id.edit_user_info) EditText info;
    @BindView(R.id.edit_user_location) MaterialEditText location;
    @BindView(R.id.edit_user_phone) MaterialEditText phone;
    @BindView(R.id.edit_user_email) MaterialEditText email;
    @BindView(R.id.edit_user_name) MaterialEditText name;
    @BindView(R.id.edit_user_dob) MaterialEditText dob;
    @BindView(R.id.edit_user_image) CircleImageView image;
    @BindView(R.id.edit_user_save_button) Button save;

    private User user;

    private String photoPath;
    private LatLng cityLocation;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_edit_user;
    }

    @Override
    protected void injectDependencies() {
        ((HasComponent<EditComponent>) getActivity()).getComponent().inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter.getUserData();

        image.setOnClickListener(view1 -> ImageUtils.openChooser(this));

        save.setOnClickListener(view1 -> {
            if (!isCorrectValue(true))
                InfoDialog.show(getActivity(), getString(R.string.attention), getString(R.string.user_empty));
        });
        presenter.setTextWatcher(info, location, phone, email, name, dob);

        location.setOnClickListener(view1 -> openAutocompleteActivity());

        dob.setOnClickListener(view1 -> presenter.startDataPicker(getActivity()));
        DAUtils.hideKeyboard(getActivity(), image);
    }

    private void setUserData() {
        setValue(info, user.shortInfo());
        setValue(location, user.location());
        setValue(phone, user.phone());
        setValue(email, user.email());
        setValue(name, user.username());
        dob.setText(DateTimeConvert.getStringDate(user.dateOfBirth()));

        photoPath = user.photo();
        if (photoPath != null) {
            Picasso.with(getActivity())
                    .load(Uri.parse(photoPath))
                    .placeholder(R.drawable.placeholder)
                    .centerCrop()
                    .fit()
                    .into(image);
        }
        DAUtils.hideKeyboard(getActivity(), image);
    }

    private void setValue(EditText editText, String s) {
        if (s != null) editText.setText(s);
    }

    @Override
    public void getUserData(User user) {
        this.user = user;

        if (user.username() != null)
            setUserData();
        else {
            save.setVisibility(View.GONE);
        }
    }

    @Override
    public void completeSaving() {
        save.setEnabled(false);
    }

    @Override
    public void changeText() {
        save.setEnabled(true);
    }

    @Override
    public void selectedDate(Date selectedDate) {
        user = User.builder(user).dateOfBirth(selectedDate).build();
        dob.setText(DateTimeConvert.getStringDate(selectedDate));
    }

    public User getNewUserData() {
        this.user = User.builder(user)
                .username(name.getText().toString())
                .email(email.getText().toString())
                .location(location.getText().toString())
                .phone(phone.getText().toString())
                .shortInfo(info.getText().toString())
                .photo(photoPath)
                .cityLocation(cityLocation)
                .build();

        return user;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == AUTOCOMPLETE_REQUEST) {
            if (resultCode == RESULT_OK) {

                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                location.setText(place.getAddress());
                cityLocation = place.getLatLng();

            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {

                Status status = PlaceAutocomplete.getStatus(getActivity(), data);
                Log.e("Autocomplete", "Error: Status = " + status.toString());

            }
        } else if (data != null && requestCode == UCrop.REQUEST_CROP) {
            save.setEnabled(true);
            Uri uri = UCrop.getOutput(data);
            photoPath = String.valueOf(uri);

            Picasso.with(getActivity())
                    .load(uri)
                    .placeholder(R.drawable.placeholder)
                    .centerCrop()
                    .fit()
                    .into(image);
        } else {
            ImageUtils.handleActivityResult(requestCode, resultCode, data, this);
        }
    }

    private void openAutocompleteActivity() {
        DAUtils.startActivityAutocomplete(this, AUTOCOMPLETE_REQUEST);
    }

    private boolean checkField(String string) {
        return string != null && !string.equals("");
    }

    public boolean isCorrectValue(boolean saving) {
        boolean check;
        if (saving)
            check = checkField(name.getText().toString());
        else
            check = checkField(user.username());

        if (saving && check)
            presenter.storeUserData(getNewUserData());

        return check;
    }
}
