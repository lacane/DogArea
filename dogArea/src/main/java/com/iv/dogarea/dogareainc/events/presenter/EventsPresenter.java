package com.iv.dogarea.dogareainc.events.presenter;


import com.alapshin.arctor.presenter.Presenter;
import com.iv.dogarea.dogareainc.events.view.EventsView;

public interface EventsPresenter extends Presenter<EventsView> {
    void getEventsData();

}
