package com.iv.dogarea.dogareainc.welcome.splash.presenter;


import com.alapshin.arctor.presenter.Presenter;
import com.iv.dogarea.dogareainc.welcome.splash.view.SplashView;

public interface SplashPresenter extends Presenter<SplashView> {
    void setupData();
    void setupUserData();
    void setupEvents();
    void setupDogs();
}
