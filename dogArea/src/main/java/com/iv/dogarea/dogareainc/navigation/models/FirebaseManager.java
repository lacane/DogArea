package com.iv.dogarea.dogareainc.navigation.models;


import com.iv.dogarea.dogareainc.navigation.view.FBAnalytics;

public interface FirebaseManager {
    FBAnalytics getAnalytics();
}
