package com.iv.dogarea.dogareainc.edit.dogsList.view;


import com.alapshin.arctor.view.MvpView;
import com.iv.dogarea.user.models.dogs.Dog;

import java.util.List;

public interface DogsListView extends MvpView {
    void getDogsData(List<Dog> dogs);
}
