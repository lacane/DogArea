package com.iv.dogarea.dogareainc.edit.editDog.presenter;


import android.content.Context;
import android.content.Intent;
import android.widget.EditText;

import com.alapshin.arctor.presenter.Presenter;
import com.iv.dogarea.dogareainc.edit.editDog.view.EditDogView;
import com.iv.dogarea.user.models.dogs.Dog;

public interface EditDogPresenter extends Presenter<EditDogView> {
    void getDogData(int index);
    void storeDogData(Dog dog);
    void setTextWatcher(EditText... texts);
    void startDataPicker(Context context);
    void setData(Intent data);
}
