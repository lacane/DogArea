package com.iv.dogarea.dogareainc.welcome.auth.view;


import com.alapshin.arctor.view.MvpView;

public interface AuthView extends MvpView {
    void nextPage();
    void previousPage();
    void emailWrong();
    void passwordWrong();
    void passwordAgainWrong();
    void authWrong();
    void successAuth();
}
