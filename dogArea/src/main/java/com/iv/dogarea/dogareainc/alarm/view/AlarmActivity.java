package com.iv.dogarea.dogareainc.alarm.view;


import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dogarea.base.mvp.view.BaseMvpActivity;
import com.iv.dogarea.R;
import com.iv.dogarea.di.HasComponent;
import com.iv.dogarea.di.components.AlarmComponent;
import com.iv.dogarea.di.components.ApplicationComponent;
import com.iv.dogarea.dogareainc.alarm.presenter.AlarmPresenter;
import com.iv.dogarea.user.models.dogs.Dog;
import com.iv.dogarea.user.models.events.Event;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.michaelevans.colorart.library.ColorArt;

import java.util.List;

import butterknife.BindView;

public class AlarmActivity extends BaseMvpActivity<AlarmView, AlarmPresenter> implements AlarmView {

    @BindView(R.id.alarm_image) ImageView imageView;
    @BindView(R.id.alarm_view) View view;
    @BindView(R.id.alarm_time) TextView time;
    @BindView(R.id.alarm_date) TextView date;
    @BindView(R.id.alarm_name) TextView name;
    @BindView(R.id.alarm_description) TextView description;

    private AlarmComponent component;
    private Event event;
    private Dog dog;
    private List<Dog> dogs;

    private int currentDog;

    private Target imageTarget = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            ColorArt colorArt = new ColorArt(bitmap);

            time.setTextColor(colorArt.getPrimaryColor());
            date.setTextColor(colorArt.getPrimaryColor());;

            view.setBackgroundColor(colorArt.getBackgroundColor());
            imageView.setImageBitmap(bitmap);
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    };

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_alarm;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void injectDependencies() {
        if (component == null)
            component = ((HasComponent<ApplicationComponent>) getApplication()).getComponent().alarmComponent();

        component.inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter.setData(getIntent());

        imageView.setOnClickListener(view1 -> {
            currentDog++;

            if (currentDog >= dogs.size())
                currentDog = 0;

            dog = dogs.get(currentDog);

            setupViews();
        });
    }

    @Override
    public void setEventData(Event event) {
        this.event = event;
        presenter.getDogsData();
    }

    @Override
    public void setDogsData(List<Dog> dogs) {
        this.dogs = dogs;
        currentDog = 0;
        dog = dogs.get(currentDog);
//        for (Dog dog: dogs) {
//            if (dog.id().equals(event.idOfDog()))
//                this.dog = dog;
//        }

        if (dog != null)
            setupViews();
    }

    private void setupViews() {
        Picasso.with(this).load(dog.photo()).into(imageTarget);
    }
}
