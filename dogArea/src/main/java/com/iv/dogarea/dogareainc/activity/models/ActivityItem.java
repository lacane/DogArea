package com.iv.dogarea.dogareainc.activity.models;


import java.util.Date;

public interface ActivityItem {
    Date getDate();
}
