package com.iv.dogarea.dogareainc.navigation.view;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;

import com.iv.dogarea.R;
import com.iv.dogarea.common.ToolbarProvider;
import com.iv.dogarea.dogareainc.activity.view.ActivityFragment;
import com.iv.dogarea.dogareainc.events.view.EventsFragment;
import com.iv.dogarea.dogareainc.navigation.models.DNType;
import com.iv.dogarea.dogareainc.navigation.models.FirebaseManager;
import com.iv.dogarea.dogareainc.profile.view.ProfileFragment;

public class DrawNavView {

    private Context context;
    private FragmentManager fragmentManager;
    private DrawerLayout drawer;
    private NavigationView.OnNavigationItemSelectedListener changeScreen = item -> {
        goToScreen(DNType.determine(item.getItemId()));
        return true;
    };

    public DrawNavView(Context context, FragmentManager fragmentManager) {
        this.context = context;
        this.fragmentManager = fragmentManager;
    }

    public void setDrawer(DrawerLayout drawer) {
        this.drawer = drawer;

        setToggle();
    }

    private void setToggle() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                (Activity) context,
                drawer,
                ((ToolbarProvider) context).getToolbar(),
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(true);

        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    public NavigationView.OnNavigationItemSelectedListener getChangeScreen() {
        return changeScreen;
    }

    public void goToScreen(DNType type) {
        goToScreen(type, true);
    }

    private void goToScreen(DNType type, boolean closeDrawer) {
        switch (type) {
            case ACTIVITY:
                changeTitle(context.getString(R.string.activities));
                startFragment(new ActivityFragment());
                break;

            case EVENTS:
                changeTitle(context.getString(R.string.events));
                startFragment(new EventsFragment());
                break;

            case PROFILE:
                changeTitle(context.getString(R.string.profile));
                startFragment(new ProfileFragment());
                break;

            case SHARE:
                ((FirebaseManager) context).getAnalytics().sendShareEvent();
                String shareBody = context.getString(R.string.share_body);
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, context.getString(R.string.app_name));
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                context.startActivity(Intent.createChooser(sharingIntent, ""));
                break;
        }

        if (closeDrawer && drawer != null)
            drawer.closeDrawer(GravityCompat.START);
    }

    private void changeTitle(String title) {
        ((Activity) context).setTitle(title);
    }

    private void startFragment(Fragment fragment) {
        fragmentManager.beginTransaction()
                .replace(R.id.navigation_container, fragment)
                .commit();
    }

    public boolean isDrawerClose() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            return false;
        } else
            return true;
    }
}
