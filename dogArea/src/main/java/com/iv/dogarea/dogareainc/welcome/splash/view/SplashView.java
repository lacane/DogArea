package com.iv.dogarea.dogareainc.welcome.splash.view;


import com.alapshin.arctor.view.MvpView;
import com.iv.dogarea.user.models.events.Event;
import com.iv.dogarea.user.models.dogs.Dog;
import com.iv.dogarea.user.models.User;

import java.util.List;

public interface SplashView extends MvpView {
    void completeUserData(User user);
    void completeEvents(List<Event> events);
    void completeDogs(List<Dog> dogs);
}
