package com.iv.dogarea.dogareainc.activity.presenter;


import com.alapshin.arctor.presenter.rx.RxPresenter;
import com.iv.dogarea.dogareainc.activity.view.ActivityView;
import com.iv.dogarea.user.models.UserModel;
import com.iv.dogarea.user.models.dogs.DogsModel;
import com.iv.dogarea.user.models.events.EventsModel;

import rx.Subscription;

public class ActivityPresenterImpl extends RxPresenter<ActivityView> implements ActivityPresenter {
    final private UserModel userModel;
    final private DogsModel dogsModel;
    final private EventsModel eventsModel;

    public ActivityPresenterImpl(UserModel userModel, DogsModel dogsModel, EventsModel eventsModel) {
        this.userModel = userModel;
        this.dogsModel = dogsModel;
        this.eventsModel = eventsModel;
    }

    @Override
    public void getUserData() {
        Subscription dataSubscription = userModel.getUserData().subscribe(user -> {
            getView().setUserData(user);
        });

        addSubscription(dataSubscription);
    }
}
