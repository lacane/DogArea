package com.iv.dogarea.dogareainc.events.presenter;


import com.alapshin.arctor.presenter.rx.RxPresenter;
import com.iv.dogarea.dogareainc.events.view.EventsView;
import com.iv.dogarea.user.models.events.EventsModel;

import rx.Subscription;

public class EventsPresenterImpl extends RxPresenter<EventsView> implements EventsPresenter {

    private final EventsModel eventsModel;

    public EventsPresenterImpl(EventsModel eventsModel) {
        this.eventsModel = eventsModel;
    }

    @Override
    public void getEventsData() {
        Subscription dataSubscription = eventsModel.getEvent().subscribe(events -> {
            getView().setupEvents(events);
        });

        addSubscription(dataSubscription);

    }
}
