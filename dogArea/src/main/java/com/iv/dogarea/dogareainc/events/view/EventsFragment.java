package com.iv.dogarea.dogareainc.events.view;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.dogarea.base.mvp.view.BaseMvpFragment;
import com.iv.dogarea.R;
import com.iv.dogarea.common.dialog.FilterEventsDialog;
import com.iv.dogarea.di.HasComponent;
import com.iv.dogarea.di.components.NavigationComponent;
import com.iv.dogarea.dogareainc.edit.editEvent.view.EditEventActivity;
import com.iv.dogarea.dogareainc.events.presenter.EventsPresenter;
import com.iv.dogarea.dogareainc.events.widget.EventsRecyclerAdapter;
import com.iv.dogarea.user.models.events.Event;

import java.util.List;

import butterknife.BindView;

public class EventsFragment extends BaseMvpFragment<EventsView, EventsPresenter>
        implements EventsView {

    private final int EDIT_EVENT_REQUEST = 1001;

    @BindView(R.id.events_recycler) RecyclerView eventsRecycler;
    @BindView(R.id.events_add) FloatingActionButton addButton;

    private EventsRecyclerAdapter adapter;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_events;
    }

    @Override
    protected void injectDependencies() {
        ((HasComponent<NavigationComponent>) getActivity()).getComponent().inject(this);
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);

        addButton.setOnClickListener(view1 ->
                startActivityForResult(new Intent(getActivity(), EditEventActivity.class), EDIT_EVENT_REQUEST));

        setupRecyclerView();

        presenter.getEventsData();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.events_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_filter:
                FilterEventsDialog dialog = new FilterEventsDialog(getContext());
                dialog.show();
                return true;
        }

        return false;
    }

    private void setupRecyclerView() {
        adapter = new EventsRecyclerAdapter(getActivity());
        adapter.setClickEvent(id -> {
            Intent editEvent = new Intent(getActivity(), EditEventActivity.class);
            editEvent.putExtra(EditEventActivity.ID_OF_EVENT, id);
            startActivityForResult(editEvent, EDIT_EVENT_REQUEST);
        });

        eventsRecycler.setAdapter(adapter);
        eventsRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        eventsRecycler.setItemAnimator(new DefaultItemAnimator());
        eventsRecycler.setHasFixedSize(true);
        eventsRecycler.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && addButton.isShown())
                    addButton.hide();
                else if (dy < 0 && !addButton.isShown())
                    addButton.show();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == EDIT_EVENT_REQUEST) {
            addButton.show();
            presenter.getEventsData();
        }
    }

    @Override
    public void setupEvents(List<Event> events) {
        adapter.setListData(events);
    }
}
