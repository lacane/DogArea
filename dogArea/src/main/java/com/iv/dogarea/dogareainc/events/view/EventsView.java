package com.iv.dogarea.dogareainc.events.view;


import com.alapshin.arctor.view.MvpView;
import com.iv.dogarea.user.models.events.Event;

import java.util.List;

public interface EventsView extends MvpView {
    void setupEvents(List<Event> events);
}
