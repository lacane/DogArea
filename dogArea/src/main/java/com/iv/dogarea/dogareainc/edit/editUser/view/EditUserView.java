package com.iv.dogarea.dogareainc.edit.editUser.view;


import com.alapshin.arctor.view.MvpView;
import com.iv.dogarea.user.models.User;

import java.util.Date;

public interface EditUserView extends MvpView {
    void getUserData(User user);
    void completeSaving();
    void changeText();
    void selectedDate(Date selectedDate);
}
