package com.iv.dogarea.dogareainc.edit.editEvent.view;


import android.os.Bundle;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;

import com.dogarea.base.mvp.view.BaseMvpActivity;
import com.iv.dogarea.R;
import com.iv.dogarea.common.dialog.InfoDialog;
import com.iv.dogarea.common.eventTypes.EventTypesView;
import com.iv.dogarea.common.eventsAlarmManager.EventAlarmManager;
import com.iv.dogarea.common.timeTypes.TimeTypesView;
import com.iv.dogarea.common.utils.DAUtils;
import com.iv.dogarea.di.HasComponent;
import com.iv.dogarea.di.components.ApplicationComponent;
import com.iv.dogarea.di.components.EditComponent;
import com.iv.dogarea.dogareainc.edit.editEvent.presenter.EditEventPresenter;
import com.iv.dogarea.user.models.dogs.Dog;
import com.iv.dogarea.user.models.events.Event;
import com.iv.dogarea.user.models.events.EventNotificationType;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import butterknife.BindView;

public class EditEventActivity extends BaseMvpActivity<EditEventView, EditEventPresenter>
        implements EditEventView {
    public static final String ID_OF_EVENT = "ID_OF_EVENT";

    @BindView(R.id.edit_event_toolbar) Toolbar toolbar;
    @BindView(R.id.edit_event_time_types_card) CardView timeTypesCard;
    @BindView(R.id.edit_event_scroll) ScrollView scrollView;
    @BindView(R.id.edit_event_time_types) TimeTypesView timeTypesView;
    @BindView(R.id.edit_event_spinner) Spinner spinner;
    @BindView(R.id.edit_event_event_type) EventTypesView eventTypesView;
    @BindView(R.id.edit_event_nothing) AppCompatRadioButton nothingButton;
    @BindView(R.id.edit_event_push) AppCompatRadioButton pushButton;
    @BindView(R.id.edit_event_alarm) AppCompatRadioButton alarmButton;
    @BindView(R.id.edit_event_description) EditText description;
    @BindView(R.id.edit_event_save_button) Button save;
    @BindView(R.id.edit_event_delete_button) Button delete;

    private EditComponent component;

    private Event event;
    private List<Dog> dogs;
    private CompoundButton.OnCheckedChangeListener checkedListener = (compoundButton, b) -> {
        if (nothingButton.isChecked())
            timeTypesCard.setVisibility(View.GONE);
        else
            timeTypesCard.setVisibility(View.VISIBLE);
    };

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_edit_event;
    }

    @Override
    @SuppressWarnings("unchecked")
    protected void injectDependencies() {
        if (component == null)
            component = ((HasComponent<ApplicationComponent>) getApplication()).getComponent().editComponent();

        component.inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        scrollView.postDelayed(() -> {
            scrollView.fullScroll(ScrollView.FOCUS_UP);
            scrollView.scrollTo(0,0);
        }, 200);

        DAUtils.hideKeyboard(this, spinner);
        scrollView.setFocusable(false);
        presenter.getDogsData();
        save.setOnClickListener(view -> {
            if (checkValues())
                presenter.storeEventData(getNewEvent());
            else
                InfoDialog.show(this, getString(R.string.attention), getString(R.string.time_type_error));
        });
        delete.setOnClickListener(view -> presenter.removeEventData());

        alarmButton.setOnCheckedChangeListener(checkedListener);
        pushButton.setOnCheckedChangeListener(checkedListener);
        nothingButton.setOnCheckedChangeListener(checkedListener);
    }

    private boolean checkValues() {
        return nothingButton.isChecked() || timeTypesView.isCorrectValues();
    }

    private Event getNewEvent() {
        String idOfDog;
        String nameDog;
        if (spinner.getSelectedItemPosition() == 0) {
            idOfDog = "all";
            nameDog = getString(R.string.all_dogs);
        }
        else {
            idOfDog = dogs.get(spinner.getSelectedItemPosition() - 1).id();
            nameDog = dogs.get(spinner.getSelectedItemPosition() - 1).name();
        }

        this.event = Event.builder(event)
                .description(description.getText().toString())
                .idOfDog(idOfDog)
                .nameDog(nameDog)
                .build();

        this.event = buildEventType();
        this.event = buildTimeType();

        return event;
    }

    private Event buildTimeType() {
        return event = Event.builder(event)
                .name(eventTypesView.getCheckedType())
                .timeType(timeTypesView.getTimeType())
                .weekTypes(timeTypesView.getWeekTypes())
                .minute(timeTypesView.getMinute())
                .hour(timeTypesView.getHour())
                .startDate(timeTypesView.getStartDate())
                .endDate(timeTypesView.getEndDate())
                .build();
    }

    private Event buildEventType() {
        EventNotificationType type;

        if (alarmButton.isChecked())
            type = EventNotificationType.ALARM;
        else if (pushButton.isChecked())
            type = EventNotificationType.NOTIFICATION;
        else
            type = EventNotificationType.NOTHING;

        return Event.builder(event)
                .eventNotificationType(type).build();
    }

    @Override
    public void getEventDataForEdit(Event event) {
        this.event = event;

        save.setText(getString(R.string.save));
        delete.setVisibility(View.VISIBLE);
        toolbar.setTitle(R.string.edit_event);

        setEventData();
    }

    private void setEventData() {
        setValue(description, event.description());
        timeTypesView.setValues(event.timeType(),
                event.weekTypes(),
                event.startDate(),
                event.endDate(),
                event.hour(),
                event.minute());
        eventTypesView.setCheckedType(event.name());

        switch (event.eventNotificationType()) {
            case ALARM:
                alarmButton.setChecked(true);
                break;

            case NOTHING:
                nothingButton.setChecked(true);
                break;

            case NOTIFICATION:
                pushButton.setChecked(true);
                break;

            default:
                break;
        }
    }

    private void setValue(EditText editText, String s) {
        if (s != null) editText.setText(s);
    }

    @Override
    public void setupDogsData(List<Dog> dogs) {
        this.dogs = dogs;
        presenter.setData(getIntent());
        setupSpinner();
    }

    private void setupSpinner() {
        List<String> dogsName = new ArrayList<>();
        dogsName.add(getString(R.string.all_dogs));

        for (Dog dog: dogs)
            dogsName.add(dog.name());

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                R.layout.support_simple_spinner_dropdown_item, dogsName);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    @Override
    public void completeSaving() {
        if (!nothingButton.isChecked())
            EventAlarmManager.createNewEvent(this, event);
        finish();
    }

    @Override
    public void completeDeleting() {
        finish();
    }

    @Override
    public void addingEvent() {
        pushButton.setChecked(true);
        event = Event.builder().id(UUID.randomUUID().hashCode()).build();
        save.setText(getString(R.string.add));
        toolbar.setTitle(R.string.add_event);
    }
}
