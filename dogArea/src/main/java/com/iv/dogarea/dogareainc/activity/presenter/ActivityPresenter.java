package com.iv.dogarea.dogareainc.activity.presenter;


import com.alapshin.arctor.presenter.Presenter;
import com.iv.dogarea.dogareainc.activity.view.ActivityView;

public interface ActivityPresenter extends Presenter<ActivityView> {
    void getUserData();
}
