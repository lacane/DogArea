package com.iv.dogarea.dogareainc.edit.view;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;

import com.dogarea.base.BaseFontActivity;
import com.iv.dogarea.R;
import com.iv.dogarea.common.dialog.InfoDialog;
import com.iv.dogarea.common.pager.PagerViewAdapter;
import com.iv.dogarea.common.utils.DAUtils;
import com.iv.dogarea.di.HasComponent;
import com.iv.dogarea.di.components.ApplicationComponent;
import com.iv.dogarea.di.components.EditComponent;
import com.iv.dogarea.dogareainc.edit.dogsList.view.DogsListFragment;
import com.iv.dogarea.dogareainc.edit.editUser.view.EditUserFragment;

import java.util.ArrayList;

import butterknife.BindView;

public class EditActivity extends BaseFontActivity
        implements HasComponent<EditComponent> {

    public static final String FIRST_EDIT = "firstEdit";

    @BindView(R.id.edit_tabs) TabLayout tabs;
    @BindView(R.id.edit_pager) ViewPager pager;
    @BindView(R.id.edit_continue) Button continueButton;

    private EditComponent component;

    private EditUserFragment editUser;
    private DogsListFragment dogsList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        PagerViewAdapter adapter = new PagerViewAdapter(getSupportFragmentManager());
        setupPager(adapter);

        pager.setAdapter(adapter);
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
                DAUtils.hideKeyboard(EditActivity.this, tabs);
            }
            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        tabs.setupWithViewPager(pager);
        tabs.setTabMode(TabLayout.MODE_FIXED);
        tabs.setTabGravity(TabLayout.GRAVITY_FILL);

        boolean firstEdit = getIntent().getBooleanExtra(FIRST_EDIT, false);

        if (firstEdit)
            continueButton.setVisibility(View.VISIBLE);
        else
            continueButton.setVisibility(View.GONE);

        continueButton.setOnClickListener(view -> {
            editUser.isCorrectValue(true);
            onBackPressed();
        });

        DAUtils.hideKeyboard(this, pager);
    }

    private void setupPager(PagerViewAdapter adapter) {
        editUser = new EditUserFragment();
        dogsList = new DogsListFragment();

        ArrayList<Fragment> fragments = new ArrayList<>();
        fragments.add(editUser);
        fragments.add(dogsList);

        ArrayList<String> titles = new ArrayList<>();
        titles.add(getString(R.string.profile_owner));
        titles.add(getString(R.string.edit_dogs));
        adapter.setTitles(titles);
        adapter.setFragmentList(fragments);
    }

    @Override
    public void onBackPressed() {
        if (editUser.isCorrectValue(false) && dogsList.isCorrectValue())
            super.onBackPressed();
        else
            InfoDialog.show(this, getString(R.string.attention),
                    getString(R.string.user_dogs_empty), getString(R.string.continue_text),
                    getString(R.string.exit), null, (dialogInterface, i) -> finish());
    }

    @Override
    @SuppressWarnings("unchecked")
    public EditComponent getComponent() {
        if (component == null)
            component = ((HasComponent<ApplicationComponent>) getApplication()).getComponent().editComponent();
        return component;
    }
}
