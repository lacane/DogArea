package com.iv.dogarea.dogareainc.profile.presenter;


import com.alapshin.arctor.presenter.rx.RxPresenter;
import com.iv.dogarea.dogareainc.profile.view.ProfileView;
import com.iv.dogarea.user.models.dogs.DogsModel;
import com.iv.dogarea.user.models.UserModel;

import rx.Subscription;

public class ProfilePresenterImpl extends RxPresenter<ProfileView> implements ProfilePresenter {
    private final UserModel userModel;
    private final DogsModel dogsModel;

    public ProfilePresenterImpl(UserModel userModel, DogsModel dogsModel) {
        this.userModel = userModel;
        this.dogsModel = dogsModel;
    }

    @Override
    public void getUserData() {
        Subscription dataSubscription = userModel.getUserData().subscribe(user -> {
            getView().setupUserData(user);
        });

        addSubscription(dataSubscription);
    }

    @Override
    public void getDogsData() {
        Subscription dataSubscription = dogsModel.getDogsData().subscribe(dogs -> {
            getView().setupDogsData(dogs);
        });

        addSubscription(dataSubscription);
    }


    @Override
    public void updateUserData() {

    }
}
