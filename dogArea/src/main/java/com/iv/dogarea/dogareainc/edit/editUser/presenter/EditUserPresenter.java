package com.iv.dogarea.dogareainc.edit.editUser.presenter;


import android.content.Context;
import android.widget.EditText;

import com.alapshin.arctor.presenter.Presenter;
import com.iv.dogarea.dogareainc.edit.editUser.view.EditUserView;
import com.iv.dogarea.user.models.User;

public interface EditUserPresenter extends Presenter<EditUserView> {
    void getUserData();
    void storeUserData(User user);
    void setTextWatcher(EditText... texts);
    void startDataPicker(Context context);
}
