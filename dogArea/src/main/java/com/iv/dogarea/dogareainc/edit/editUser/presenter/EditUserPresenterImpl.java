package com.iv.dogarea.dogareainc.edit.editUser.presenter;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.alapshin.arctor.presenter.rx.RxPresenter;
import com.iv.dogarea.common.utils.DateTimePicker;
import com.iv.dogarea.dogareainc.edit.editUser.view.EditUserView;
import com.iv.dogarea.user.models.User;
import com.iv.dogarea.user.models.UserModel;

import org.joda.time.DateTime;

import rx.Subscription;

public class EditUserPresenterImpl extends RxPresenter<EditUserView> implements EditUserPresenter {
    private final UserModel userModel;

    private DatePickerDialog.OnDateSetListener pickerCallback = (datePicker, year, monthOfYear, dayOfMonth) -> {
        getView().selectedDate(new DateTime(year, monthOfYear + 1, dayOfMonth, 0, 0).toDate());
    };

    public EditUserPresenterImpl(UserModel userModel) {
        this.userModel = userModel;
    }

    @Override
    public void getUserData() {
        Subscription dataSubscription = userModel.getUserData().subscribe(user -> {
            getView().getUserData(user);
        });

        addSubscription(dataSubscription);
    }

    @Override
    public void storeUserData(User user) {
        Subscription dataSubscription = userModel.setUser(user).subscribe(user1 -> {
            userModel.storeUserData().subscribe(user2 -> {
                getView().completeSaving();
            });
        });

        addSubscription(dataSubscription);
    }

    @Override
    public void setTextWatcher(EditText... editTexts) {
        for (EditText editText: editTexts)
            editText.addTextChangedListener(getTextWatcher());
    }

    @Override
    public void startDataPicker(Context context) {
        DateTimePicker.startDateSimplePicker(context, pickerCallback, ((Activity) context).getCurrentFocus());
    }

    private TextWatcher getTextWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                getView().changeText();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
    }
}
