package com.iv.dogarea.dogareainc.welcome.splash.view;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.dogarea.base.mvp.view.BaseMvpFragment;
import com.iv.dogarea.R;
import com.iv.dogarea.di.HasComponent;
import com.iv.dogarea.di.components.WelcomeComponent;
import com.iv.dogarea.user.models.events.Event;
import com.iv.dogarea.dogareainc.navigation.view.NavigationActivity;
import com.iv.dogarea.dogareainc.welcome.splash.presenter.SplashPresenter;
import com.iv.dogarea.user.models.dogs.Dog;
import com.iv.dogarea.user.models.User;

import java.util.List;

import butterknife.BindView;

public class SplashFragment extends BaseMvpFragment<SplashView, SplashPresenter> implements SplashView {

    @BindView(R.id.splash_info)
    TextView userInfo;

    private boolean userComplete, eventsComplete, dogsComplete = false;

    private List<Dog> dogs;
    private User user;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_splash;
    }

    @Override
    protected void injectDependencies() {
        ((HasComponent<WelcomeComponent>) getActivity()).getComponent().inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        presenter.setupData();
    }

    private void checkCompleted() {
        if (userComplete && eventsComplete && dogsComplete) {
            userInfo.postDelayed(() -> {
                boolean correctValues = user != null &&  user.username() != null && dogs.size() != 0;

                Intent mainIntent = new Intent(getActivity(), NavigationActivity.class);
                mainIntent.putExtra(NavigationActivity.CORRECT_VALUES, correctValues);
                startActivity(mainIntent);

                getActivity().finish();
            }, 0);

        }
    }

    @Override
    public void completeUserData(User user) {
        this.user = user;
        String text = userInfo.getText().toString();
        if (user != null)
            userInfo.setText(text + "Юзер загружен: " + user + "\n");
        else
            userInfo.setText(text + "Юзер не зарегистрирован\n");

        userComplete = true;
        checkCompleted();
    }

    @Override
    public void completeEvents(List<Event> events) {
        String text = userInfo.getText().toString();
        if (events != null)
            userInfo.setText(text + "Event загружены:" + events + "\n");
        else
            userInfo.setText(text + "Event не найдены\n");

        eventsComplete = true;
        checkCompleted();

    }

    @Override
    public void completeDogs(List<Dog> dogs) {
        this.dogs = dogs;

        String hash = "";

        for (Dog dog: dogs) {
            hash += " " + dog.id();
        }

        Log.w("IDS", hash);

        String text = userInfo.getText().toString();
        if (dogs != null)
            userInfo.setText(text + "Dogs загружены:" + dogs + "\n");
        else
            userInfo.setText(text + "Dogs не найдены\n");

        dogsComplete = true;
        checkCompleted();
    }
}
