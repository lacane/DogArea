package com.iv.dogarea.dogareainc.welcome.view;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.dogarea.base.BaseFontActivity;
import com.iv.dogarea.R;
import com.iv.dogarea.common.ToolbarProvider;
import com.iv.dogarea.di.HasComponent;
import com.iv.dogarea.di.components.ApplicationComponent;
import com.iv.dogarea.di.components.WelcomeComponent;
import com.iv.dogarea.dogareainc.welcome.models.WNType;
import com.iv.dogarea.dogareainc.welcome.models.WelcomeNavigator;

import butterknife.BindView;

public class WelcomeActivity extends BaseFontActivity
        implements HasComponent<WelcomeComponent>, WelcomeNavigator, ToolbarProvider {

    private WelcomeComponent component;
    private WelcomeNavigatorView navigator;

    @BindView(R.id.welcome_toolbar) Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

//        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        navigator = new WelcomeNavigatorView(this, getSupportFragmentManager());
        navigator.goToScreen(WNType.AUTH);
    }

    @Override
    @SuppressWarnings("unchecked")
    public WelcomeComponent getComponent() {
        if (component == null)
            component = ((HasComponent<ApplicationComponent>) getApplication())
                    .getComponent().welcomeComponent();

        return component;
    }

    @Override
    public WelcomeNavigatorView getNavigator() {
        return navigator;
    }

    @Override
    public Toolbar getToolbar() {
        return toolbar;
    }
}
