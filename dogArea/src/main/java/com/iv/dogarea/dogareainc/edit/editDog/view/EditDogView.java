package com.iv.dogarea.dogareainc.edit.editDog.view;


import com.alapshin.arctor.view.MvpView;
import com.iv.dogarea.user.models.dogs.Dog;

import java.util.Date;

public interface EditDogView extends MvpView {
    void getDogDataForEdit(Dog dog);
    void selectedDate(Date selectedDate);
    void changeText();
    void completeSaving();
    void addingDog();
}
