package com.iv.dogarea.dogareainc.edit.dogsList.view;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;

import com.dogarea.base.mvp.view.BaseMvpFragment;
import com.iv.dogarea.R;
import com.iv.dogarea.common.dogPreview.view.DogPreview;
import com.iv.dogarea.di.HasComponent;
import com.iv.dogarea.di.components.EditComponent;
import com.iv.dogarea.dogareainc.edit.dogsList.presenter.DogsListPresenter;
import com.iv.dogarea.dogareainc.edit.editDog.view.EditDogActivity;
import com.iv.dogarea.user.models.dogs.Dog;

import java.util.List;

import butterknife.BindView;

public class DogsListFragment extends BaseMvpFragment<DogsListView, DogsListPresenter>
        implements DogsListView {

    private final int EDIT_DOG_REQUEST = 1;

    @BindView(R.id.edit_dogs_add_button) Button addDogs;
    @BindView(R.id.edit_dogs_list) DogPreview preview;
    @BindView(R.id.edit_dogs_add_button_fab) FloatingActionButton addDogsFab;
    @BindView(R.id.edit_dogs_scroll) ScrollView scrollView;

    private int scrollY;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_edit_dogs;
    }

    @Override
    protected void injectDependencies() {
        ((HasComponent<EditComponent>) getActivity()).getComponent().inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        addDogsFab.hide();
        presenter.getDogsData();

        addDogs.setOnClickListener(view1 ->
                startActivityForResult(new Intent(getActivity(), EditDogActivity.class), EDIT_DOG_REQUEST));

        addDogsFab.setOnClickListener(view1 ->
                startActivityForResult(new Intent(getActivity(), EditDogActivity.class), EDIT_DOG_REQUEST));

        scrollY = -1;
        scrollView.getViewTreeObserver().addOnScrollChangedListener(() -> {
            if (scrollY == -1)
                scrollY = scrollView.getScrollY();
            else {
                if (scrollY - scrollView.getScrollY() > 0)
                    addDogsFab.hide();
                else
                    addDogsFab.show();
                scrollY = scrollView.getScrollY();
            }
        });
    }

    @Override
    public void getDogsData(List<Dog> dogs) {
        preview.setOnClickDogItemListener(index -> {
            Intent editDog = new Intent(getContext(), EditDogActivity.class);
            editDog.putExtra(EditDogActivity.INDEX_OF_DOG, index);
            startActivityForResult(editDog, EDIT_DOG_REQUEST);
        });
        preview.setupDogsData(dogs);

        if (dogs.size() > 0) {
            addDogs.setVisibility(View.GONE);
            addDogsFab.setVisibility(View.VISIBLE);
        } else {
            addDogs.setVisibility(View.VISIBLE);
            addDogsFab.setVisibility(View.GONE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == EDIT_DOG_REQUEST) {
            presenter.getDogsData();
        }
    }

    public boolean isCorrectValue() {
        return preview.isCorrectValue();
    }
}
