package com.iv.dogarea.dogareainc.profile.view;


import com.alapshin.arctor.view.MvpView;
import com.iv.dogarea.user.models.dogs.Dog;
import com.iv.dogarea.user.models.User;

import java.util.List;

public interface ProfileView extends MvpView {
    void setupUserData(User user);
    void setupDogsData(List<Dog> dogs);
}