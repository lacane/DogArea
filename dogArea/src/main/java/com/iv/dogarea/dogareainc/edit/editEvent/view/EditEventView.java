package com.iv.dogarea.dogareainc.edit.editEvent.view;


import com.alapshin.arctor.view.MvpView;
import com.iv.dogarea.user.models.dogs.Dog;
import com.iv.dogarea.user.models.events.Event;

import java.util.List;

public interface EditEventView extends MvpView {
    void getEventDataForEdit(Event event);
    void setupDogsData(List<Dog> dogs);
    void completeSaving();
    void completeDeleting();
    void addingEvent();
}
