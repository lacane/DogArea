package com.iv.dogarea.dogareainc.profile.view;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dogarea.base.mvp.view.BaseMvpFragment;
import com.iv.dogarea.R;
import com.iv.dogarea.common.DogSwitcherView;
import com.iv.dogarea.common.InfoTextView;
import com.iv.dogarea.common.ToolbarProvider;
import com.iv.dogarea.common.utils.DAUtils;
import com.iv.dogarea.common.utils.DateTimeConvert;
import com.iv.dogarea.di.HasComponent;
import com.iv.dogarea.di.components.NavigationComponent;
import com.iv.dogarea.dogareainc.edit.view.EditActivity;
import com.iv.dogarea.dogareainc.navigation.models.DrawerNavigator;
import com.iv.dogarea.dogareainc.profile.presenter.ProfilePresenter;
import com.iv.dogarea.user.models.dogs.Dog;
import com.iv.dogarea.user.models.User;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileFragment extends BaseMvpFragment<ProfileView, ProfilePresenter>
        implements ProfileView {

    private final int EDIT_REQUEST = 0;
    private boolean isFinish;

    @BindView(R.id.profile_dog_switcher) DogSwitcherView dogSwitcherView;
    @BindView(R.id.profile_user_name) TextView userName;
    @BindView(R.id.profile_user_short_info) TextView userInfo;
    @BindView(R.id.profile_user_image) CircleImageView userImage;
    @BindView(R.id.profile_menu) ImageView menuButton;

    @BindView(R.id.profile_location) InfoTextView location;
    @BindView(R.id.profile_email) InfoTextView email;
    @BindView(R.id.profile_phone) InfoTextView phone;

    @BindView(R.id.profile_edit_button) FloatingActionButton editButton;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_profile;
    }

    @Override
    protected void injectDependencies() {
        ((HasComponent<NavigationComponent>) getActivity()).getComponent().inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        isFinish = false;

        ((ToolbarProvider) getActivity()).getToolbar().setVisibility(View.GONE);

        menuButton.setOnClickListener(view1 -> ((DrawerNavigator) getActivity()).openDrawer());

        editButton.setOnClickListener(view1 ->
                startActivityForResult(new Intent(getActivity(), EditActivity.class), EDIT_REQUEST));

        presenter.getDogsData();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        ((ToolbarProvider) getActivity()).getToolbar().setVisibility(View.VISIBLE);
    }

    @Override
    public void setupUserData(User user) {

        checkCorrectValues(user.username() != null);

        userName.setText(DAUtils.getNameAge(user.username(),
                DateTimeConvert.getAge(getActivity(), user.dateOfBirth())));

        userInfo.setText(user.shortInfo());

        if (user.photo() != null) {
            Picasso.with(getActivity())
                    .load(Uri.parse(user.photo()))
                    .placeholder(R.drawable.placeholder)
                    .centerCrop()
                    .fit()
                    .into(userImage);
        }

        location.setText(user.location());
        email.setText(user.email());
        phone.setText(user.phone());

        if (user.username() != null)
            ((DrawerNavigator) getActivity()).updateUser(user);
    }

    @Override
    public void setupDogsData(List<Dog> dogs) {
        if (checkCorrectValues(dogs.size() != 0)) {
            dogSwitcherView.setDogs(dogs);
            presenter.getUserData();
        }
    }

    private boolean checkCorrectValues(boolean check) {
        if (!check && !isFinish) {
            Intent editProfile = new Intent(getActivity(), EditActivity.class);
            editProfile.putExtra(EditActivity.FIRST_EDIT, true);
            startActivityForResult(editProfile, EDIT_REQUEST);
        }
        else if (!check)
            getActivity().finish();

        return check;
    }

    @Override
    public void onStop() {
        super.onStop();
        dogSwitcherView.setEnabled(false);
    }

    @Override
    public void onPause() {
        super.onPause();
        dogSwitcherView.setEnabled(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        dogSwitcherView.setEnabled(true);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == EDIT_REQUEST) {
            isFinish = true;
            presenter.getDogsData();
        }
    }
}
