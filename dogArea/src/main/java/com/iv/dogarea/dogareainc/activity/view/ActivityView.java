package com.iv.dogarea.dogareainc.activity.view;


import com.alapshin.arctor.view.MvpView;
import com.iv.dogarea.user.models.User;

public interface ActivityView extends MvpView {
    void setUserData(User user);
}
