package com.iv.dogarea.dogareainc.activity.view;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.dogarea.base.mvp.view.BaseMvpFragment;
import com.iv.dogarea.R;
import com.iv.dogarea.di.HasComponent;
import com.iv.dogarea.di.components.NavigationComponent;
import com.iv.dogarea.dogareainc.activity.presenter.ActivityPresenter;
import com.iv.dogarea.dogareainc.navigation.models.DrawerNavigator;
import com.iv.dogarea.user.models.User;

import butterknife.BindView;

public class ActivityFragment extends BaseMvpFragment<ActivityView, ActivityPresenter>
        implements ActivityView {

    @BindView(R.id.activity_text)
    TextView textView;

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_activity;
    }

    @Override
    protected void injectDependencies() {
        ((HasComponent<NavigationComponent>) getActivity()).getComponent().inject(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter.getUserData();
    }

    @Override
    public void setUserData(User user) {
        ((DrawerNavigator) getActivity()).updateUser(user);
    }
}
