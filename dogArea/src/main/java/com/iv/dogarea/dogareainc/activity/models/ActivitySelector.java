package com.iv.dogarea.dogareainc.activity.models;


import android.content.Context;

import com.iv.dogarea.dogareainc.activity.view.ActivityEventView;
import com.iv.dogarea.user.models.events.Event;

import java.util.Date;

public class ActivitySelector implements ActivityItem {

    private ActivityType type;
    private ActivityItem item;

    public ActivitySelector() {
        this.type = ActivityType.EVENT;
    }

    public ActivityEventView createEvent(Context context, Event event) {
        type = ActivityType.EVENT;
        ActivityEventView view = new ActivityEventView(context).setEvent(event);
        item = view;
        return view;
    }

    public ActivityType getType() {
        return type;
    }

    @Override
    public Date getDate() {
        return item.getDate();
    }
}
