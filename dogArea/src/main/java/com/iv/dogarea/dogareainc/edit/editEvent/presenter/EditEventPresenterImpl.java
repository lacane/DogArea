package com.iv.dogarea.dogareainc.edit.editEvent.presenter;


import android.content.Intent;

import com.alapshin.arctor.presenter.rx.RxPresenter;
import com.iv.dogarea.dogareainc.edit.editEvent.view.EditEventActivity;
import com.iv.dogarea.dogareainc.edit.editEvent.view.EditEventView;
import com.iv.dogarea.user.models.dogs.DogsModel;
import com.iv.dogarea.user.models.events.Event;
import com.iv.dogarea.user.models.events.EventsModel;

import rx.Subscription;

public class EditEventPresenterImpl extends RxPresenter<EditEventView> implements EditEventPresenter {
    private final EventsModel eventsModel;
    private final DogsModel dogsModel;
    private int indexData;
    private boolean editEvent;

    public EditEventPresenterImpl(EventsModel eventsModel, DogsModel dogsModel) {
        this.eventsModel = eventsModel;
        this.dogsModel = dogsModel;
    }

    @Override
    public void getEventData(int id) {
        Subscription dataSubscription = eventsModel.getEvent().subscribe(events -> {

            indexData = -1;
            for (int index = 0; index < events.size(); index++) {
                if (id == events.get(index).id()) {
                    indexData = index;
                    getView().getEventDataForEdit(events.get(index));
                    editEvent = true;
                }
            }

            if (indexData == -1)
                getView().addingEvent();

        });

        addSubscription(dataSubscription);

    }

    @Override
    public void getDogsData() {
        Subscription dataSubscription = dogsModel.getDogsData().subscribe(dogs -> {
            getView().setupDogsData(dogs);
        });

        addSubscription(dataSubscription);
    }

    @Override
    public void storeEventData(Event event) {
        Subscription dataSubscription;
        if (!editEvent) {
            dataSubscription = eventsModel.setEvents(event).subscribe(events -> {
                eventsModel.storeEvents().subscribe(events1 -> {
                    getView().completeSaving();
                });
            });
        } else {
            dataSubscription = eventsModel.changeEventData(event, indexData).subscribe(events -> {
                eventsModel.storeEvents().subscribe(events1 -> {
                    getView().completeSaving();
                });
            });
        }

        addSubscription(dataSubscription);

    }

    @Override
    public void setData(Intent data) {
        editEvent = false;

        if (data != null) {
            Integer id = data.getIntExtra(EditEventActivity.ID_OF_EVENT, -1);

            if (id == -1)
                getView().addingEvent();
            else
                getEventData(id);

        } else
            getView().addingEvent();

    }

    @Override
    public void removeEventData() {
        Subscription dataSubscription = eventsModel.removeEventData(indexData).subscribe(events -> {
            getView().completeDeleting();
        });

        addSubscription(dataSubscription);
    }
}
