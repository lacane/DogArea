package com.iv.dogarea.dogareainc.edit.editEvent.presenter;


import android.content.Intent;

import com.alapshin.arctor.presenter.Presenter;
import com.iv.dogarea.dogareainc.edit.editEvent.view.EditEventView;
import com.iv.dogarea.user.models.events.Event;

public interface EditEventPresenter extends Presenter<EditEventView> {
    void getEventData(int id);
    void getDogsData();
    void storeEventData(Event event);
    void setData(Intent data);
    void removeEventData();
}
