package com.iv.dogarea.dogareainc.navigation.models;


import com.iv.dogarea.R;

public enum DNType {

    ACTIVITY(R.id.nav_activities),
    PROFILE(R.id.nav_profile),
    SHARE(R.id.nav_share),
    EVENTS(R.id.nav_events),
    SEND(R.id.nav_send);

    private int id;

    DNType(int id) {
        this.id = id;
    }

    public static DNType determine(int itemId) {
        for (DNType type: DNType.values()) {
            if (itemId == type.id)
                return type;
        }
        return ACTIVITY;
    }
}
