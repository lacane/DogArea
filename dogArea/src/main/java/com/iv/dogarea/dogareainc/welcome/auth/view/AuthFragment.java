package com.iv.dogarea.dogareainc.welcome.auth.view;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;

import com.dogarea.base.mvp.view.BaseMvpFragment;
import com.iv.dogarea.R;
import com.iv.dogarea.common.ToolbarProvider;
import com.iv.dogarea.common.dialog.InfoDialog;
import com.iv.dogarea.di.HasComponent;
import com.iv.dogarea.di.components.WelcomeComponent;
import com.iv.dogarea.dogareainc.welcome.auth.presenter.AuthPresenter;
import com.iv.dogarea.dogareainc.welcome.models.WNType;
import com.iv.dogarea.dogareainc.welcome.models.WelcomeNavigator;

import butterknife.BindView;

public class AuthFragment extends BaseMvpFragment<AuthView, AuthPresenter>
        implements AuthView {

//    @BindView(R.id.auth_password) EditText authPassword;
//    @BindView(R.id.auth_email) EditText authEmail;
//    @BindView(R.id.auth_enter) Button enter;
//
//    @BindView(R.id.auth_sign_up_password) EditText signUpPassword;
//    @BindView(R.id.auth_sign_up_password_again) EditText signUpPasswordAgain;
//    @BindView(R.id.auth_sign_up_email) EditText signUpEmail;
//    @BindView(R.id.auth_begin) Button begin;
//
//    @BindView(R.id.auth_flipper) ViewFlipper flipper;
//    @BindView(R.id.auth_tabs) TabLayout tabs;

    @BindView(R.id.auth_test_email)
    EditText email;

    private Animation nextIn;
    private Animation nextOut;
    private Animation prevIn;
    private Animation prevOut;

    @Override
    protected int getLayoutRes() {
        return R.layout.auth_test;
    }

    @Override
    protected void injectDependencies() {
        ((HasComponent<WelcomeComponent>) getActivity()).getComponent().inject(this);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        nextIn = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_left_in);
        nextOut = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_left_out);

        prevIn = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_right_in);
        prevOut = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_right_out);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//
//        tabs.addOnTabSelectedListener(presenter.getTabListener());
//
//        enter.setOnClickListener(view1 -> {
//            DAUtils.hideKeyboard(getActivity(), tabs);
//
//            presenter.login(authEmail.getText().toString(),
//                    authPassword.getText().toString());
//        });
//
//        begin.setOnClickListener(view1 -> {
//            DAUtils.hideKeyboard(getActivity(), tabs);
//
//            presenter.signUp(signUpEmail.getText().toString(),
//                    signUpPassword.getText().toString(),
//                    signUpPasswordAgain.getText().toString());
//        });
//
//        authEmail.setText("xameleonicus@gmail.com");
//        authPassword.setText("kolokola");
//
        ((ToolbarProvider) getActivity()).getToolbar().setVisibility(View.GONE);
//        button.setOnClickListener(view1 ->
//                mAuth.createUserWithEmailAndPassword(authEmail.getText().toString(), authPassword.getText().toString())
//                .addOnCompleteListener(this));
//
    }

    @Override
    public void nextPage() {
//        flipper.setInAnimation(nextIn);
//        flipper.setOutAnimation(nextOut);
//        flipper.showNext();
    }

    @Override
    public void previousPage() {
//        flipper.setInAnimation(prevIn);
//        flipper.setOutAnimation(prevOut);
//        flipper.showPrevious();
    }

    @Override
    public void emailWrong() {
        InfoDialog.show(getActivity(), getString(R.string.attention), getString(R.string.email_error));
    }

    @Override
    public void passwordWrong() {
        InfoDialog.show(getActivity(), getString(R.string.attention), getString(R.string.password_error));
    }

    @Override
    public void passwordAgainWrong() {
        InfoDialog.show(getActivity(), getString(R.string.attention), getString(R.string.password_again_error));
    }

    @Override
    public void authWrong() {
        InfoDialog.show(getActivity(), getString(R.string.error), getString(R.string.auth_error));
    }

    @Override
    public void successAuth() {
        ((WelcomeNavigator) getActivity()).getNavigator().goToScreen(WNType.SPLASH, true);
    }
}
