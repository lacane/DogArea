package com.iv.dogarea.dogareainc.events.widget;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.iv.dogarea.user.models.events.Event;

import java.util.ArrayList;
import java.util.List;

public class EventsRecyclerAdapter extends RecyclerView.Adapter<EventsRecyclerAdapter.ViewHolder> {
    private Context context;
    private List<Event> listData;

    private OnClickEvent clickEvent;

    public EventsRecyclerAdapter(Context context) {
        this.context = context;
        listData = new ArrayList<>();
    }

    public void setClickEvent(OnClickEvent clickEvent) {
        this.clickEvent = clickEvent;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        EventItemView itemView = new EventItemView(context);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Event event = listData.get(position);
        ((EventItemView) holder.itemView).setEvent(event);
        ((EventItemView) holder.itemView).setClickEvent(clickEvent);
        ((EventItemView) holder.itemView).bind();
    }

    public void setListData(List<Event> listData) {
        this.listData = listData;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ViewHolder(EventItemView itemView) {
            super(itemView);
        }
    }

    public interface OnClickEvent {
        void onClick(Integer id);
    }
}
