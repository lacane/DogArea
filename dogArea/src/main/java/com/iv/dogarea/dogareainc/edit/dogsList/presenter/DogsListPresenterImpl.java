package com.iv.dogarea.dogareainc.edit.dogsList.presenter;


import com.alapshin.arctor.presenter.rx.RxPresenter;
import com.iv.dogarea.dogareainc.edit.dogsList.view.DogsListView;
import com.iv.dogarea.user.models.dogs.DogsModel;

import rx.Subscription;

public class DogsListPresenterImpl extends RxPresenter<DogsListView> implements DogsListPresenter {
    private final DogsModel dogsModel;

    public DogsListPresenterImpl(DogsModel dogsModel) {
        this.dogsModel = dogsModel;
    }

    @Override
    public void getDogsData() {
        Subscription dataSubscription = dogsModel.getDogsData().subscribe(dogs -> {
            getView().getDogsData(dogs);
        });

        addSubscription(dataSubscription);
    }
}
