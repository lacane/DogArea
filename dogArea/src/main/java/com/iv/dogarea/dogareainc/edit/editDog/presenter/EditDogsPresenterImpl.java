package com.iv.dogarea.dogareainc.edit.editDog.presenter;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.alapshin.arctor.presenter.rx.RxPresenter;
import com.iv.dogarea.common.utils.DateTimePicker;
import com.iv.dogarea.dogareainc.edit.editDog.view.EditDogActivity;
import com.iv.dogarea.dogareainc.edit.editDog.view.EditDogView;
import com.iv.dogarea.user.models.dogs.Dog;
import com.iv.dogarea.user.models.dogs.DogsModel;

import org.joda.time.DateTime;

import rx.Subscription;

public class EditDogsPresenterImpl extends RxPresenter<EditDogView> implements EditDogPresenter {
    private final DogsModel dogsModel;
    private int indexData;
    private boolean editDog;

    private DatePickerDialog.OnDateSetListener pickerCallback = (datePicker, year, monthOfYear, dayOfMonth) -> {
        getView().selectedDate(new DateTime(year, monthOfYear + 1, dayOfMonth, 0, 0).toDate());
    };

    public EditDogsPresenterImpl(DogsModel dogsModel) {
        this.dogsModel = dogsModel;
    }

    @Override
    public void getDogData(int index) {
        Subscription dataSubscription = dogsModel.getDogsData().subscribe(dogs -> {

            if (index >= dogs.size())
                getView().addingDog();
            else {
                getView().getDogDataForEdit(dogs.get(index));
                editDog = true;
            }
        });

        addSubscription(dataSubscription);
    }

    @Override
    public void storeDogData(Dog dog) {
        Subscription dataSubscription;
        if (!editDog) {
            dataSubscription = dogsModel.setDogData(dog).subscribe(dogs -> {
                dogsModel.storeDogsData().subscribe(dogs1 -> {
                    getView().completeSaving();
                });
            });
        } else {
            dataSubscription = dogsModel.changeDogData(dog, indexData).subscribe(dogs -> {
                dogsModel.storeDogsData().subscribe(dogs1 -> {
                    getView().completeSaving();
                });
            });
        }

        addSubscription(dataSubscription);
    }

    @Override
    public void setTextWatcher(EditText... editTexts) {
        for (EditText editText: editTexts)
            editText.addTextChangedListener(getTextWatcher());

    }

    @Override
    public void startDataPicker(Context context) {
        DateTimePicker.startDateSimplePicker(context, pickerCallback, ((Activity) context).getCurrentFocus());
    }

    @Override
    public void setData(Intent data) {
        editDog = false;

        if (data != null) {
            indexData = data.getIntExtra(EditDogActivity.INDEX_OF_DOG, -1);

            if (indexData == -1)
                getView().addingDog();
            else
                getDogData(indexData);

        } else
            getView().addingDog();
    }

    private TextWatcher getTextWatcher() {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                getView().changeText();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
    }
}
