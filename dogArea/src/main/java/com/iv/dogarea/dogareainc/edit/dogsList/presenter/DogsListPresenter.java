package com.iv.dogarea.dogareainc.edit.dogsList.presenter;


import com.alapshin.arctor.presenter.Presenter;
import com.iv.dogarea.dogareainc.edit.dogsList.view.DogsListView;

public interface DogsListPresenter extends Presenter<DogsListView> {
    void getDogsData();
}
