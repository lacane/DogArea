package com.iv.dogarea.dogareainc.profile.presenter;


import com.alapshin.arctor.presenter.Presenter;
import com.iv.dogarea.dogareainc.profile.view.ProfileView;

public interface ProfilePresenter extends Presenter<ProfileView> {
    void getUserData();
    void getDogsData();
    void updateUserData();
}
