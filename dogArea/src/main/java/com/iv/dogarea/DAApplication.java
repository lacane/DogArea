package com.iv.dogarea;


import com.crashlytics.android.Crashlytics;
import com.iv.dogarea.common.storage.StorageService;
import com.iv.dogarea.di.components.ApplicationComponent;

import net.danlew.android.joda.JodaTimeAndroid;

import io.fabric.sdk.android.Fabric;
import pl.aprilapps.easyphotopicker.EasyImage;

public class DAApplication extends DogAreaApplication {

    private ApplicationComponent component;
    static DAApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        JodaTimeAndroid.init(this);
//        DateTimeZone.setDefault(DateTimeZone.forTimeZone(TimeZone.getDefault()));
        Fabric.with(this, new Crashlytics());
        instance = this;

        StorageService.instance(this);
        setupDagger();
        setupEasyImage();
    }

    private void setupEasyImage() {
        EasyImage.configuration(this)
                .setImagesFolderName(getString(R.string.app_name))
                .saveInRootPicturesDirectory();
    }


    @Override
    protected void setupDagger() {
        if (component == null) {
            component = ApplicationComponent.Builder.build(this);
        }
        component.inject(this);

    }

    public static ApplicationComponent component() {
        return instance.getComponent();
    }

    @Override
    public ApplicationComponent getComponent() {
        return component;
    }
}
