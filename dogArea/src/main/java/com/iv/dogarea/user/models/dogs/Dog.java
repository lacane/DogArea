package com.iv.dogarea.user.models.dogs;

import android.support.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.auto.value.AutoValue;
import com.iv.dogarea.user.models.Gender;

import java.util.Date;

@AutoValue
public abstract class Dog {

    public static Builder builder(Dog dog) {
        return new AutoValue_Dog.Builder(dog);
    }

    public static Builder builder() {
        return new AutoValue_Dog.Builder();
    }

    @JsonProperty
    public abstract String id();

    @JsonProperty
    @Nullable
    public abstract Gender gender();

    @JsonProperty
    @Nullable
    public abstract String name();

    @JsonProperty
    @Nullable
    public abstract String photo();

    @JsonProperty
    @Nullable
    public abstract Date dateOfBirth();

    @JsonProperty
    @Nullable
    public abstract String race();

    @JsonProperty
    @Nullable
    public abstract String info();

    @AutoValue.Builder
    public abstract static class Builder {

        @JsonProperty
        public abstract Builder id(String id);
        @JsonProperty
        public abstract Builder name(String name);
        @JsonProperty
        public abstract Builder photo(String photo);
        @JsonProperty
        public abstract Builder dateOfBirth(Date dateOfBirth);
        @JsonProperty
        public abstract Builder race(String race);
        @JsonProperty
        public abstract Builder info(String info);
        @JsonProperty
        public abstract Builder gender(Gender gender);

        public abstract Dog build();
    }
}
