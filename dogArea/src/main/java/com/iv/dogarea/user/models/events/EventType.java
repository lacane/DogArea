package com.iv.dogarea.user.models.events;

import android.content.Context;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.iv.dogarea.R;

public enum EventType {
    @JsonProperty
    OTHER,
    @JsonProperty
    WALK,
    @JsonProperty
    FEED,
    @JsonProperty
    CLINIC,
    @JsonProperty
    DEWORMING,
    @JsonProperty
    VACCINATION,
    @JsonProperty
    SHOP,
    @JsonProperty
    HUNTER,
    @JsonProperty
    WORKOUT,
    @JsonProperty
    GROOMING,
    @JsonProperty
    THERAPY,
    @JsonProperty
    DOCUMENTS;

    public String getName(Context context) {
        switch (this) {
            case OTHER:
                return context.getString(R.string.other);

            case WALK:
                return context.getString(R.string.walk);

            case FEED:
                return context.getString(R.string.feed);

            case CLINIC:
                return context.getString(R.string.clinic);

            case DEWORMING:
                return context.getString(R.string.deworming);

            case VACCINATION:
                return context.getString(R.string.vaccination);

            case SHOP:
                return context.getString(R.string.shop);

            case HUNTER:
                return context.getString(R.string.hunter);

            case WORKOUT:
                return context.getString(R.string.workout);

            case GROOMING:
                return context.getString(R.string.grooming);

            case THERAPY:
                return context.getString(R.string.therapy);

            case DOCUMENTS:
                return context.getString(R.string.documents);

            default:
                return super.toString();
        }
    }
}