package com.iv.dogarea.user.models;


import rx.Observable;

public interface UserModel {
    Observable<User> getUserData();

    Observable<User> storeUserData();

    Observable<User> setUser(User user);
}
