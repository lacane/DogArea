package com.iv.dogarea.user.models.events;


import java.util.List;

import rx.Observable;

public interface EventsModel {
    Observable<List<Event>> getEvent();

    Observable<List<Event>> storeEvents();

    Observable<List<Event>> setEvents(List<Event> events);

    Observable<List<Event>> setEvents(Event event);

    Observable<List<Event>> changeEventData(Event event, int index);

    Observable<List<Event>> removeEventData(int index);
}
