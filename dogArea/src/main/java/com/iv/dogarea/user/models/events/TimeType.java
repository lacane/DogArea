package com.iv.dogarea.user.models.events;

import android.content.Context;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.iv.dogarea.R;

public enum TimeType {
    @JsonProperty
    ONE_TIME,
    @JsonProperty
    EVERYDAY,
    @JsonProperty
    EVERY_WEEK,
    @JsonProperty
    RANGE,
    @JsonProperty
    EVERY_YEAR;

    public String getName(Context context) {
        switch (this) {

            case ONE_TIME:
                return context.getString(R.string.one_time);

            case EVERYDAY:
                return context.getString(R.string.everyday);

            case EVERY_WEEK:
                return context.getString(R.string.every_week);

            case EVERY_YEAR:
                return context.getString(R.string.every_year);

            case RANGE:
                return context.getString(R.string.range);

            default:
                return "";
        }
    }
}