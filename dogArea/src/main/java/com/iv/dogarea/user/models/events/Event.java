package com.iv.dogarea.user.models.events;

import android.support.annotation.Nullable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.auto.value.AutoValue;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@AutoValue
public abstract class Event implements Serializable {

    public static Builder builder(Event event) {
        return new AutoValue_Event.Builder(event);
    }

    public static Builder builder() {
        return new AutoValue_Event.Builder();
    }

    @JsonProperty
    public abstract Integer id();

    @JsonProperty
    @Nullable
    public abstract EventType name();

    @JsonProperty
    @Nullable
    public abstract String description();

    @JsonProperty
    @Nullable
    public abstract Date startDate();

    @JsonProperty
    @Nullable
    public abstract Date endDate();

    @JsonProperty
    @Nullable
    public abstract Integer hour();

    @JsonProperty
    @Nullable
    public abstract Integer minute();

    @JsonProperty
    @Nullable
    public abstract List<WeekType> weekTypes();

    @JsonProperty
    @Nullable
    public abstract EventNotificationType eventNotificationType();

    @JsonProperty
    @Nullable
    public abstract TimeType timeType();

    @JsonProperty
    @Nullable
    public abstract String idOfDog();

    @JsonProperty
    @Nullable
    public abstract String nameDog();

    @AutoValue.Builder
    public abstract static class Builder {

        @JsonProperty
        public abstract Builder id(Integer id);

        @JsonProperty
        public abstract Builder name(EventType name);

        @JsonProperty
        public abstract Builder description(String description);

        @JsonProperty
        public abstract Builder idOfDog(String idOfDog);

        @JsonProperty
        public abstract Builder nameDog(String nameDog);

        @JsonProperty
        public abstract Builder startDate(Date startDate);

        @JsonProperty
        public abstract Builder endDate(Date endDate);

        @JsonProperty
        public abstract Builder weekTypes(List<WeekType> weekTypes);

        @JsonProperty
        public abstract Builder hour(Integer hour);

        @JsonProperty
        public abstract Builder minute(Integer minute);

        @JsonProperty
        public abstract Builder eventNotificationType(EventNotificationType eventNotificationType);

        @JsonProperty
        public abstract Builder timeType(TimeType timeType);

        public abstract Event build();
    }

}
