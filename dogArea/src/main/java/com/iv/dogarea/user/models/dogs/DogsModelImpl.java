package com.iv.dogarea.user.models.dogs;


import com.iv.dogarea.common.storage.StorageService;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

public class DogsModelImpl implements DogsModel {

    private List<Dog> dogs = new ArrayList<>();

    @Override
    public Observable<List<Dog>> getDogsData() {
        if (dogs == null)
            dogs = new ArrayList<>();
        return Observable.just(dogs);
    }

    @Override
    public Observable<List<Dog>> storeDogsData() {
        StorageService.storeDogs(dogs);
        return Observable.just(dogs);
    }

    @Override
    public Observable<List<Dog>> setDogData(Dog dog) {
        dogs.add(dog);
        return Observable.just(dogs);
    }

    @Override
    public Observable<List<Dog>> changeDogData(Dog dog, int index) {
        if (index >= 0 && index < dogs.size()) {
            dogs.remove(index);
            dogs.add(index, dog);
        }
        return Observable.just(dogs);
    }

    @Override
    public Observable<List<Dog>> setDogsData(List<Dog> dogs) {
        this.dogs = dogs;
        return Observable.just(dogs);
    }
}
