package com.iv.dogarea.user.models;


import com.iv.dogarea.common.storage.StorageService;

import rx.Observable;

public class UserModelImpl implements UserModel {

    private User user = User.builder().build();

    @Override
    public Observable<User> getUserData() {
        if (user == null)
            user = User.builder().build();
        return Observable.just(user);
    }

    @Override
    public Observable<User> storeUserData() {
        StorageService.storeUser(user);
        return Observable.just(user);
    }

    @Override
    public Observable<User> setUser(User user) {
        this.user = user;
        return Observable.just(user);
    }
}
