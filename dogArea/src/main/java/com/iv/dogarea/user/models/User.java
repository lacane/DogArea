package com.iv.dogarea.user.models;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.android.gms.maps.model.LatLng;
import com.google.auto.value.AutoValue;

import java.util.Date;

import javax.annotation.Nullable;

@AutoValue
public abstract class User {

    public static Builder builder(User user) {
        return new AutoValue_User.Builder(user);
    }

    public static Builder builder() {
        return new AutoValue_User.Builder();
    }

    @JsonProperty
    @Nullable
    public abstract String username();

    @JsonProperty
    @Nullable
    public abstract String location();

    @JsonProperty
    @Nullable
    public abstract String email();

    @JsonProperty
    @Nullable
    public abstract String phone();

    @JsonProperty
    @Nullable
    public abstract Date dateOfBirth();

    @JsonProperty
    @Nullable
    public abstract String shortInfo();

    @JsonProperty
    @Nullable
    public abstract String photo();

    @JsonProperty
    @Nullable
    public abstract LatLng cityLocation();

    @AutoValue.Builder
    public abstract static class Builder {

        @JsonProperty
        public abstract Builder username(String username);

        @JsonProperty
        public abstract Builder location(String location);

        @JsonProperty
        public abstract Builder email(String email);

        @JsonProperty
        public abstract Builder phone(String phone);

        @JsonProperty
        public abstract Builder dateOfBirth(Date date);

        @JsonProperty
        public abstract Builder shortInfo(String shortInfo);

        @JsonProperty
        public abstract Builder photo(String photo);

        @JsonProperty
        public abstract Builder cityLocation(LatLng cityLocation);

        public abstract User build();
    }

}
