package com.iv.dogarea.user.models.events;


import com.fasterxml.jackson.annotation.JsonProperty;

public enum WeekType {
    @JsonProperty
    MONDAY,
    @JsonProperty
    TUESDAY,
    @JsonProperty
    WEDNESDAY,
    @JsonProperty
    THURSDAY,
    @JsonProperty
    FRIDAY,
    @JsonProperty
    SATURDAY,
    @JsonProperty
    SUNDAY;

    public Integer getDay() {
        switch (this) {

            case MONDAY:
                return 1;

            case TUESDAY:
                return 2;

            case WEDNESDAY:
                return 3;

            case THURSDAY:
                return 4;

            case FRIDAY:
                return 5;

            case SATURDAY:
                return 6;

            case SUNDAY:
                return 7;


            default:
                return -1;
        }
    }
}
