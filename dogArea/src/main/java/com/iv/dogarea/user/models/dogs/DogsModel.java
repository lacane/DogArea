package com.iv.dogarea.user.models.dogs;


import java.util.List;

import rx.Observable;

public interface DogsModel {
    Observable<List<Dog>> getDogsData();

    Observable<List<Dog>> storeDogsData();

    Observable<List<Dog>> setDogData(Dog dog);

    Observable<List<Dog>> changeDogData(Dog dog, int index);

    Observable<List<Dog>> setDogsData(List<Dog> dogs);
}
