package com.iv.dogarea.user.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Gender {
    @JsonProperty
    M,
    @JsonProperty
    W,
}