package com.iv.dogarea.user.models.events;


import com.iv.dogarea.common.storage.StorageService;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

public class EventsModelImpl implements EventsModel {

    private List<Event> events = new ArrayList<>();

    public Observable<List<Event>> getEvent() {
        if (events == null)
            events = new ArrayList<>();
        return Observable.just(events);
    }

    @Override
    public Observable<List<Event>> storeEvents() {
        StorageService.storeEvents(events);
        return Observable.just(events);
    }

    @Override
    public Observable<List<Event>> setEvents(List<Event> events) {
        this.events = events;
        return Observable.just(events);
    }

    @Override
    public Observable<List<Event>> setEvents(Event event) {
        if (event != null)
            events.add(event);
        return Observable.just(events);
    }

    @Override
    public Observable<List<Event>> changeEventData(Event event, int index) {

        if (event != null && index >= 0 && index < events.size()) {
            events.remove(index);
            events.add(index, event);
        }
        return Observable.just(events);

    }

    @Override
    public Observable<List<Event>> removeEventData(int index) {

        if (index >= 0 && index < events.size()) {
            events.remove(index);
            StorageService.storeEvents(events);
        }

        return Observable.just(events);
    }

}
