package com.iv.dogarea.user.models.events;

import android.content.Context;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.iv.dogarea.R;

public enum EventNotificationType {
    @JsonProperty
    NOTIFICATION,
    @JsonProperty
    ALARM,
    @JsonProperty
    NOTHING;

    public String getName(Context context) {
        switch (this) {

            case NOTIFICATION:
                return context.getString(R.string.push_notification);

            case ALARM:
                return context.getString(R.string.alarm);

            case NOTHING:
                return context.getString(R.string.nothing);

            default:
                return "";
        }
    }
}