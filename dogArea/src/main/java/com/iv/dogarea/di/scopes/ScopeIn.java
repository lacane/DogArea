package com.iv.dogarea.di.scopes;

import javax.inject.Scope;

@Scope
public @interface ScopeIn {
    Class<?> value();
}
