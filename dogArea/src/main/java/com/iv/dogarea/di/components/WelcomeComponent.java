package com.iv.dogarea.di.components;

import com.iv.dogarea.di.modules.WelcomeModule;
import com.iv.dogarea.di.scopes.ScopeIn;
import com.iv.dogarea.dogareainc.welcome.auth.view.AuthFragment;
import com.iv.dogarea.dogareainc.welcome.splash.view.SplashFragment;

import dagger.Subcomponent;

@ScopeIn(WelcomeComponent.class)
@Subcomponent(modules = WelcomeModule.class)
public interface WelcomeComponent {
    void inject(SplashFragment fragment);
    void inject(AuthFragment fragment);
}
