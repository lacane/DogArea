package com.iv.dogarea.di.modules;

import com.iv.dogarea.di.components.ApplicationComponent;
import com.iv.dogarea.di.scopes.ScopeIn;
import com.iv.dogarea.user.models.events.EventsModel;
import com.iv.dogarea.user.models.events.EventsModelImpl;
import com.iv.dogarea.user.models.dogs.DogsModel;
import com.iv.dogarea.user.models.dogs.DogsModelImpl;
import com.iv.dogarea.user.models.UserModel;
import com.iv.dogarea.user.models.UserModelImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class UserDataModule {

    @Provides
    @ScopeIn(ApplicationComponent.class)
    UserModel provideUser() {
        return new UserModelImpl();
    }

    @Provides
    @ScopeIn(ApplicationComponent.class)
    EventsModel provideEventsModel() {
        return new EventsModelImpl();
    }

    @Provides
    @ScopeIn(ApplicationComponent.class)
    DogsModel provideDogsModel() {
        return new DogsModelImpl();
    }

}
