package com.iv.dogarea.di.components;

import com.iv.dogarea.di.modules.NavigationModule;
import com.iv.dogarea.di.scopes.ScopeIn;
import com.iv.dogarea.dogareainc.activity.view.ActivityFragment;
import com.iv.dogarea.dogareainc.events.view.EventsFragment;
import com.iv.dogarea.dogareainc.navigation.view.NavigationActivity;
import com.iv.dogarea.dogareainc.profile.view.ProfileFragment;

import dagger.Subcomponent;

@ScopeIn(NavigationComponent.class)
@Subcomponent(modules = NavigationModule.class)
public interface NavigationComponent {
    void inject(NavigationActivity activity);
    void inject(ActivityFragment fragment);
    void inject(EventsFragment fragment);
    void inject(ProfileFragment fragment);
}
