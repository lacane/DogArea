package com.iv.dogarea.di.modules;

import com.iv.dogarea.di.components.AlarmComponent;
import com.iv.dogarea.di.scopes.ScopeIn;
import com.iv.dogarea.dogareainc.alarm.presenter.AlarmPresenter;
import com.iv.dogarea.dogareainc.alarm.presenter.AlarmPresenterImpl;
import com.iv.dogarea.user.models.dogs.DogsModel;

import dagger.Module;
import dagger.Provides;

@Module
public abstract class AlarmModule {

    @Provides
    @ScopeIn(AlarmComponent.class)
    static AlarmPresenter provideAlarmPresenter(DogsModel dogsModel) {
        return new AlarmPresenterImpl(dogsModel);
    }
}
