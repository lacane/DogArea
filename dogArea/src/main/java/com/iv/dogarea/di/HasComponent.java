package com.iv.dogarea.di;

public interface HasComponent<C> {
    C getComponent();
}