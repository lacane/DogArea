package com.iv.dogarea.di.modules;


import android.app.Application;
import android.content.Context;

import com.iv.dogarea.di.components.ApplicationComponent;
import com.iv.dogarea.di.scopes.ScopeIn;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {
    private final Application app;

    public ApplicationModule(Application app) {
        this.app = app;
    }

    @Provides
    @ScopeIn(ApplicationComponent.class)
    Application provideApplication() {
        return this.app;
    }

    @Provides
    @ScopeIn(ApplicationComponent.class)
    Context provideApplicationContext() {
        return this.app;
    }
}
