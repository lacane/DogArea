package com.iv.dogarea.di.modules;

import com.iv.dogarea.di.components.NavigationComponent;
import com.iv.dogarea.di.scopes.ScopeIn;
import com.iv.dogarea.dogareainc.activity.presenter.ActivityPresenter;
import com.iv.dogarea.dogareainc.activity.presenter.ActivityPresenterImpl;
import com.iv.dogarea.dogareainc.events.presenter.EventsPresenter;
import com.iv.dogarea.dogareainc.events.presenter.EventsPresenterImpl;
import com.iv.dogarea.dogareainc.profile.presenter.ProfilePresenter;
import com.iv.dogarea.dogareainc.profile.presenter.ProfilePresenterImpl;
import com.iv.dogarea.user.models.dogs.DogsModel;
import com.iv.dogarea.user.models.UserModel;
import com.iv.dogarea.user.models.events.EventsModel;

import dagger.Module;
import dagger.Provides;

@Module
public abstract class NavigationModule {

    @Provides
    @ScopeIn(NavigationComponent.class)
    static ActivityPresenter provideActivityPresenter(UserModel userModel,
                                                      DogsModel dogsModel, EventsModel eventsModel) {
        return new ActivityPresenterImpl(userModel, dogsModel, eventsModel);
    }

    @Provides
    @ScopeIn(NavigationComponent.class)
    static EventsPresenter provideEventsPresenter(EventsModel eventsModel) {
        return new EventsPresenterImpl(eventsModel);
    }

    @Provides
    @ScopeIn(NavigationComponent.class)
    static ProfilePresenter provideProfilePresenter(UserModel userModel, DogsModel dogsModel) {
        return new ProfilePresenterImpl(userModel, dogsModel);
    }
}
