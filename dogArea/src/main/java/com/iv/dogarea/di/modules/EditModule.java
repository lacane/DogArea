package com.iv.dogarea.di.modules;

import com.iv.dogarea.di.components.EditComponent;
import com.iv.dogarea.di.scopes.ScopeIn;
import com.iv.dogarea.dogareainc.edit.dogsList.presenter.DogsListPresenter;
import com.iv.dogarea.dogareainc.edit.dogsList.presenter.DogsListPresenterImpl;
import com.iv.dogarea.dogareainc.edit.editDog.presenter.EditDogPresenter;
import com.iv.dogarea.dogareainc.edit.editDog.presenter.EditDogsPresenterImpl;
import com.iv.dogarea.dogareainc.edit.editEvent.presenter.EditEventPresenter;
import com.iv.dogarea.dogareainc.edit.editEvent.presenter.EditEventPresenterImpl;
import com.iv.dogarea.dogareainc.edit.editUser.presenter.EditUserPresenter;
import com.iv.dogarea.dogareainc.edit.editUser.presenter.EditUserPresenterImpl;
import com.iv.dogarea.user.models.dogs.DogsModel;
import com.iv.dogarea.user.models.UserModel;
import com.iv.dogarea.user.models.events.EventsModel;

import dagger.Module;
import dagger.Provides;

@Module
public abstract class EditModule {

    @Provides
    @ScopeIn(EditComponent.class)
    static DogsListPresenter provideEditDogsPresenter(DogsModel dogsModel) {
        return new DogsListPresenterImpl(dogsModel);
    }

    @Provides
    @ScopeIn(EditComponent.class)
    static EditUserPresenter provideEditUserPresenter(UserModel userModel) {
        return new EditUserPresenterImpl(userModel);
    }

    @Provides
    @ScopeIn(EditComponent.class)
    static EditDogPresenter provideEditDogPresenter(DogsModel dogsModel) {
        return new EditDogsPresenterImpl(dogsModel);
    }

    @Provides
    @ScopeIn(EditComponent.class)
    static EditEventPresenter provideEditEventPresenter(EventsModel eventsModel, DogsModel dogsModel) {
        return new EditEventPresenterImpl(eventsModel, dogsModel);
    }
}
