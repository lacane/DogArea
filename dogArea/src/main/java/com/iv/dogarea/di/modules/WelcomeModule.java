package com.iv.dogarea.di.modules;

import com.iv.dogarea.di.components.WelcomeComponent;
import com.iv.dogarea.di.scopes.ScopeIn;
import com.iv.dogarea.dogareainc.welcome.auth.presenter.AuthPresenter;
import com.iv.dogarea.dogareainc.welcome.auth.presenter.AuthPresenterImpl;
import com.iv.dogarea.dogareainc.welcome.splash.presenter.SplashPresenter;
import com.iv.dogarea.dogareainc.welcome.splash.presenter.SplashPresenterImpl;
import com.iv.dogarea.user.models.UserModel;
import com.iv.dogarea.user.models.dogs.DogsModel;
import com.iv.dogarea.user.models.events.EventsModel;

import dagger.Module;
import dagger.Provides;

@Module
public abstract class WelcomeModule {

    @Provides
    @ScopeIn(WelcomeComponent.class)
    static SplashPresenter provideSplashPresenter(UserModel model, EventsModel eventsModel,
                                                  DogsModel dogsModel) {
        return new SplashPresenterImpl(model, eventsModel, dogsModel);
    }

    @Provides
    @ScopeIn(WelcomeComponent.class)
    static AuthPresenter provideAuthPresenter() {
        return new AuthPresenterImpl();
    }
}
