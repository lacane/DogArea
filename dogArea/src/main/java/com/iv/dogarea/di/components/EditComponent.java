package com.iv.dogarea.di.components;

import com.iv.dogarea.di.modules.EditModule;
import com.iv.dogarea.di.scopes.ScopeIn;
import com.iv.dogarea.dogareainc.edit.dogsList.view.DogsListFragment;
import com.iv.dogarea.dogareainc.edit.editDog.view.EditDogActivity;
import com.iv.dogarea.dogareainc.edit.editEvent.view.EditEventActivity;
import com.iv.dogarea.dogareainc.edit.editUser.view.EditUserFragment;
import com.iv.dogarea.dogareainc.edit.view.EditActivity;

import dagger.Subcomponent;

@ScopeIn(EditComponent.class)
@Subcomponent(modules = EditModule.class)
public interface EditComponent {
    void inject(EditActivity activity);
    void inject(EditEventActivity activity);
    void inject(EditDogActivity activity);
    void inject(EditUserFragment fragment);
    void inject(DogsListFragment fragment);
}
