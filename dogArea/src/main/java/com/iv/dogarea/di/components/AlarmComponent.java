package com.iv.dogarea.di.components;

import com.iv.dogarea.di.modules.AlarmModule;
import com.iv.dogarea.di.scopes.ScopeIn;
import com.iv.dogarea.dogareainc.alarm.view.AlarmActivity;

import dagger.Subcomponent;

@ScopeIn(AlarmComponent.class)
@Subcomponent(modules = AlarmModule.class)
public interface AlarmComponent {
    void inject(AlarmActivity activity);
}
