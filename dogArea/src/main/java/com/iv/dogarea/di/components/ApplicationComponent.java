package com.iv.dogarea.di.components;


import com.iv.dogarea.DAApplication;
import com.iv.dogarea.common.storage.StorageService;
import com.iv.dogarea.di.modules.ApplicationModule;
import com.iv.dogarea.di.modules.UserDataModule;
import com.iv.dogarea.di.scopes.ScopeIn;

import dagger.Component;

@ScopeIn(ApplicationComponent.class)
@Component(modules = {
        ApplicationModule.class,
        UserDataModule.class,
})
public interface ApplicationComponent {
    final class Builder {
        public static ApplicationComponent build(DAApplication app) {
            return DaggerApplicationComponent.builder()
                    .applicationModule(new ApplicationModule(app))
                    .build();
        }

        private Builder() {}
    }

    AlarmComponent alarmComponent();
    NavigationComponent navigationComponent();
    WelcomeComponent welcomeComponent();
    EditComponent editComponent();

    void inject(DAApplication application);
    void inject(StorageService service);
}
